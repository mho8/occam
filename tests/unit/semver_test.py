import unittest

import os, random
random.seed(os.environ["PYTHONHASHSEED"])

from occam.semver import Semver

from unittest.mock import patch


class Test__init__(unittest.TestCase):
    pass


class TestSemver:
  @staticmethod
  def createInputList(fix = 0, fixed = None, returnLargest=False, returnRandom=False):
    # Create a random list of numbers
    values = list(range(0, 100))
    random.shuffle(values)

    # Get the smallest value (by index)
    compareFunction = min
    if returnLargest:
      compareFunction = max
    if returnRandom:
      compareFunction = random.choice

    index = values.index(compareFunction(values))

    # For each 'fixed' value, determine its value randomly
    if fixed is None:
      fixed = [random.randint(0, 100) for _ in range(fix)]

    # Create input list
    input = ["{}.{}.{}.{}".format(*fixed, x, *[random.randint(0,9) for _ in range(4-len(fixed))]) for x in values]

    return input, index

  class TestSortVersionList(unittest.TestCase):

    def test_should_return_a_list(self):
      self.assertTrue(isinstance(Semver.sortVersionList(["1.0", "1.0.1", "0.9"]), list))

    def test_should_return_a_list_of_the_same_length_as_the_input(self):
      length = random.randint(1, 10)
      input  = ["1.0"] * length
      self.assertCountEqual(Semver.sortVersionList(input), input)

    def test_should_sort_major_versions(self):
      input, smallest = TestSemver.createInputList()

      result = Semver.sortVersionList(input)

      self.assertEqual(result[0], input[smallest])

    def test_should_sort_minor_versions(self):
      input, smallest = TestSemver.createInputList(fix = 1)

      result = Semver.sortVersionList(input)

      self.assertEqual(result[0], input[smallest])

    def test_should_sort_patch_versions(self):
      input, smallest = TestSemver.createInputList(fix = 2)

      result = Semver.sortVersionList(input)

      self.assertEqual(result[0], input[smallest])

    def test_should_sort_revision_versions(self):
      input, smallest = TestSemver.createInputList(fix = 3)

      result = Semver.sortVersionList(input)

      self.assertEqual(result[0], input[smallest])

    def test_should_sort_patches_as_greater_than_base_versions(self):
      input = ["{}.{}.{}".format(*[random.randint(0,9) for _ in range(3)])] * 3
      input[1] = input[1] + ".p1"
      input[2] = input[2] + ".p2"
      random.shuffle(input)

      result = Semver.sortVersionList(input)

      self.assertNotEqual(result[0][-2], "p")

    def test_should_sort_patches_as_less_than_numbered_versions(self):
      input = ["{}.{}.{}".format(*[random.randint(0,9) for _ in range(3)])] * 3
      input[0] = input[0] + ".{}".format(random.randint(0, 9))
      input[1] = input[1] + ".p1"
      input[2] = input[2] + ".p2"
      random.shuffle(input)

      result = Semver.sortVersionList(input)

      self.assertEqual(result[0][-2], "p")

    def test_should_sort_truncated_values_as_though_they_had_zeros(self):
      a = random.randint(0, 9)
      b = a + 1

      input, _ = TestSemver.createInputList(fixed = [a])
      input.append("{}".format(b))
      largest = input[-1]
      random.shuffle(input)

      result = Semver.sortVersionList(input)

      self.assertEqual(result[-1], largest)

    def test_should_return_an_empty_list_when_input_is_empty(self):
      self.assertEqual(Semver.sortVersionList([]), [])

    def test_should_return_input_list_when_input_is_a_single_item(self):
      self.assertEqual(Semver.sortVersionList(["a"]), ["a"])

  class TestResolveVersion(unittest.TestCase):
    def test_should_work_for_exact_versions(self):
      input, _ = TestSemver.createInputList()

      find = random.choice(input)

      self.assertEqual(Semver.resolveVersion(find, input), find)

    def test_should_handle_greater_than_or_equal(self):
      input, largest = TestSemver.createInputList(returnLargest=True)

      find = ">=" + random.choice(input)

      self.assertEqual(Semver.resolveVersion(find, input), input[largest])

    def test_should_handle_less_than_or_equal(self):
      input, median = TestSemver.createInputList(returnRandom=True)

      find = "<=" + input[median]

      self.assertEqual(Semver.resolveVersion(find, input), input[median])

    def test_should_handle_greater_than(self):
      input = ["4", "4.4", "4.4.p1", "4.4.0", "4.4.1", "5"]
      result = input[-1]
      find = ">" + input[2]

      random.shuffle(input)

      self.assertEqual(Semver.resolveVersion(find, input), result)

    def test_should_handle_less_than(self):
      input = ["4", "4.4", "4.4.p1", "4.4.0", "4.4.1", "5"]
      result = input[2]
      find = "<" + input[3]

      random.shuffle(input)

      self.assertEqual(Semver.resolveVersion(find, input), result)

    def test_should_handle_wildcards(self):
      input = ["1.0.0", "4", "4.4", "4.4.p1", "4.4.0", "4.4.1", "5"]
      result = "1.0.0"
      find = "1.x"

      random.shuffle(input)

      self.assertEqual(Semver.resolveVersion(find, input), result)

    # TODO: test for range sets
