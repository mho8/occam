# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.links.manager import LinkManager
from occam.objects.manager  import ObjectManager

from occam.commands.manager import command, option, argument

@command('links', 'list',
  category      = 'Link Management',
  documentation = "Lists targets for a particular relationship for the given object.")
@argument("source", type="object")
@option("-r", "--relationship", action = "store",
                                dest = "relationship",
                                type = str,
                                help = "filter by relationship")
@option("-d", "--destination",  action = "store",
                                dest = "destination",
                                type = "object",
                                help = "filter by destination object")
@option("-j", "--json",  dest    = "to_json",
                         action  = "store_true",
                         help    = "returns result as a json document")
@option("-o", "--order", dest    = "order",
                         default = "ascending",
                         action  = "store",
                         help    = "Orders the links in the given order (ascending = default, descending)")
@uses(LinkManager)
@uses(ObjectManager)
class ListCommand:
  def do(self):
    Log.header("Listing all %s links" % self.options.relationship)

    sources = self.objects.search(uuid = self.options.source.id)

    if len(sources) == 0:
      Log.error("Cannot find source object (%s)" % (self.options.source.id))
      return -1

    source_db = sources[0]

    destination_db = None
    if self.options.destination:
      targets = self.objects.search(uuid = self.options.destination.id)
      if len(targets) == 0:
        Log.error("Cannot find destination object (%s)" % (self.options.destination.id))
        return -1

      destination_db = targets[0]

    links = self.links.retrieveObjects(source_db, self.options.relationship, target_object_db = destination_db, order = self.options.order)

    ret = []

    for link in links:
      link = link._data

      link["id"] = link["uid"]
      del link["uid"]

      link["revision"] = link["target_revision"]
      del link["target_revision"]

      link["type"] = link["object_type"]
      del link["object_type"]

      link["tags"] = link["tags"][1:-1].split(';')
      link["capabilities"] = link["capabilities"][1:-1].split(';')

      link["relationship"] = link["relationship"]

      ret.append(link)

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      Log.output(ret)

    return 0
