# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.permissions.manager    import PermissionManager

@command('configurations', 'new',
  category      = 'Configuration Management',
  documentation = "Displays the metadata for the object.")
@argument("object",   type = "object",
                      help = "The object to create the configuration from")
@argument("index",    type = int,
                      help = "The index of the input for the configuration to create")
@option("-n", "--name", dest   = "name",
                        action = "store",
                        help   = "the name for the new configuration object")
@option("-t", "--to",   dest   = "to_object",
                        action = "store",
                        type   = "object",
                        help   = "the object to add the new object as a dependency")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
@uses(PermissionManager)
class NewCommand:
  def do(self):
    # Get the object to create a configuration from
    obj = self.objects.resolve(self.options.object ,person = self.person)

    if obj is None:
      Log.error("cannot find object with id %s" % (self.options.object.id))
      return -1

    info = self.objects.infoFor(obj)

    objName = info.get('name', 'unknown')

    Log.write("Creating configuration for %s" % (objName))

    # Pull out the schema, which will be stored within the configuration object
    schema = self.configurations.schemaFor(obj, self.options.index)

    # Initial configuration is empty
    data = {}

    new_obj = None

    # Create a configuration object
    object_info = {}

    object_info["subtype"] = ["application/json"]
    object_info["file"]    = "data.json"
    object_info["schema"]  = {
      "file": info["inputs"][self.options.index]["schema"],
      "revision": obj.revision,
      "id": info["id"],
      "name": info.get('name', 'unknown'),
      "type": info.get('type', 'object')
    }

    name = self.options.name or "Configuration for %s" % (objName)

    if self.options.to_object:
      obj = self.objects.retrieve(uuid=self.options.to_object.id, object_options=self.options.to_object, person = self.person)

      if obj is None:
        Log.error("Cannot find the root object to add this object to.")
        return -1

      # Add a new object to the given object
      root, obj, path = self.objects.temporaryClone(obj, person = self.person)
      new_obj = self.objects.write.addObjectTo(obj, name=name, object_type="configuration", info = object_info)
    else:
      new_obj = self.objects.write.create(name=name, object_type="configuration", info = object_info, root=None, path=None)

    if new_obj is None:
      Log.error("Could not create the configuration object.")
      return -1

    # Add the default configuration files
    self.objects.write.addFileTo(new_obj, object_info["file"], json.dumps(data))
    # TODO: add an embed schema option
    #self.objects.write.addFileTo(new_obj, object_info["schema"], json.dumps(schema))
    self.objects.write.commit(new_obj, message="Adds configuration files")

    # Store the configuration object
    self.objects.write.store(new_obj)

    ret = {}
    ret["updated"] = []

    for x in (new_obj.roots or []):
      ret["updated"].append({
        "id": x.uuid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": new_obj.uuid,
      "revision": new_obj.revision,
      "position": new_obj.position,
    })

    # Set initial permissions
    self.permissions.update(new_obj.uuid, canRead=False, canWrite=False, canClone=False)
    self.permissions.update(new_obj.uuid, person_uuid=self.person.uuid, canRead=True, canWrite=True, canClone=True)

    if self.options.to_json:
      Log.output(json.dumps(ret))
    else:
      Log.write("new object id: ", end ="")
      Log.output("%s" % (self.objects.infoFor(new_obj)['id']), padding="")
      Log.write("new object revision: ", end ="")
      Log.output("%s" % (new_obj.revision), padding="")

    Log.done("Successfully created %s" % (self.objects.infoFor(new_obj).get('name')))
    return 0
