# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import shlex

from occam.config     import Config
from occam.log        import loggable

from occam.manager import manager, uses

from occam.storage.manager   import StorageManager
from occam.backends.manager  import BackendManager
from occam.system.manager    import SystemManager
from occam.notes.manager     import NoteManager
from occam.objects.manager   import ObjectManager
from occam.databases.manager import DatabaseManager

from occam.objects.write_manager import ObjectWriteManager

@loggable
@uses(StorageManager)
@uses(BackendManager)
@uses(SystemManager)
@uses(NoteManager)
@uses(ObjectManager)
@uses(ObjectWriteManager)
@uses(DatabaseManager)
@manager("jobs")
class JobManager:
  """ This OCCAM manager handles dispatching and controlling running tasks.
  """

  drivers = {}

  @staticmethod
  def register(adapter, driverClass):
    """ Adds a new scheduler driver to the possible drivers.
    """

    JobManager.drivers[adapter] = driverClass

  def driverFor(self, adapter):
    """ Returns an instance of a driver for the given scheduler backend.
    """

    if not adapter in self.drivers:
      # TODO: error?
      adapter = 'occam'

    return self.drivers[adapter](self.configuration)

  def __init__(self):
    """ Initialize the job manager.
    """

    self._driver = None

  def connect(self):
    """ Initializes a job scheduler connection.
    """

    # Determine the database type
    if self._driver is None:
      adapter = self.configuration.get('adapter', 'occam')

      # Attempt to find that database driver
      import importlib
      
      try:
        importlib.import_module("occam.jobs.plugins.%s" % (adapter))
      except ImportError:
        pass

      self._driver = self.driverFor(adapter)

    return self._driver

  def _jobThatRunsThisWorkflow(self, run_id):
    import sql
    jobRun = self.jobRunFor(runId=run_id)
    return self.retrieve(kind="workflow", job_id= jobRun.job_id)


  def jobRunFor(self, runId=None, jobId=None):
    import sql
    jobRun = sql.Table("job_run")

    session = self.database.session()


    query = jobRun.select()
    if runId is not None:
      query.where = (jobRun.run_id == runId)
    if jobId is not None:
      query.where = (jobRun.job_id == jobId)
      # This is not great :)
      if runId is not None:
        query.where = query.where & (jobRun.run_id == runId)
    self.database.execute(session, query)
    row = self.database.fetch(session)

    if not row:
      return None

    from occam.jobs.records.job_run import JobRunRecord

    return JobRunRecord(row)


  def updateRunSection(self, task, runSection, root = None):
    """ Modifies the given task to replace the run section with the given run section.
    """
    if root is None:
      root = task

    # For this object, add its install paths to root
    if 'index' in task and task['index'] == root['runs']['index']:
      task['run'] = task.get('run', {})
      task['run'].update(runSection)
      return root

    for wire in task.get('inputs', []) + [{"connections": task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        # Recursively find paths to subobjects
        self.updateRunSection(inputObject, runSection, root)

    return root

  def writeTask(self, task, taskPath):
    import json

    # Create an stdin file/socket to communicate to the object
    stdinPath = os.path.join(taskPath, "stdin")
    if not os.path.exists(stdinPath):
      with open(stdinPath, "w+") as f:
        pass

    # Output the whole task to path/object.json
    taskManifestPath = os.path.join(taskPath, "task", "object.json")

    if not os.path.exists(taskManifestPath):
      f = open(taskManifestPath, "w+")
      json.dump(task, f, indent=2, separators=(',', ': '))
      f.close()

  def writeNetwork(self, network, taskPath):
    import json

    # Output the whole task to path/object.json
    networkManifestPath = os.path.join(taskPath, "network.json")

    if not os.path.exists(networkManifestPath):
      f = open(networkManifestPath, "w+")
      json.dump(network, f, indent=2, separators=(',', ': '))
      f.close()

  def _deploy(self, task, taskPath, cwd=None, pathsFor=[], buildObject=None, buildPath=None, runSection = None, person = None, stdin = None, stdout = None, interactive=False):
    """ Deploys the given task.
    """

    # Ask the StorageManager for paths for each object at its revision
    paths = self.pathsForTask(task,
                              buildObject    = buildObject,
                              buildPath      = buildPath,
                              person         = person)

    rootPath = os.path.join(taskPath, "task", "root")
    self.installTask(rootPath, task, task, paths)

    for k, v in paths.items():
      for item in pathsFor:
        if item.get('id') == v.get('id') and item.get('revision') == v.get('revision'):
          v['path'] = item['path']

      if v.get('index') == task.get('runs', task.get('builds', {})).get('index'):
        if not cwd is None:
          v['localPath']  = cwd
          localMount  = task.get('runs', task.get('builds', {})).get('paths', {}).get('localMount')
          localMounts = task.get('runs', task.get('builds', {})).get('paths', {}).get('local')
          if len(localMounts) > 0:
            localMount = localMounts.get(task.get('provides').get('architecture'), {}).get(task.get('provides').get('environment'), localMount)
          v['localMount'] = os.path.join(localMount, "local")

        # Install stdin file if we have data
        if stdin is not None:
          runningTaskLocal = os.path.join(taskPath, "objects", str(v['index']))
          if not os.path.exists(runningTaskLocal):
            try:
              os.makedirs(runningTaskLocal)
            except OSError as exc:  # Python >2.5
              if exc.errno == errno.EEXIST and os.path.isdir(runningTaskLocal):
                pass
              else:
                raise
          stdinPath = os.path.join(runningTaskLocal, "stdin")
          with open(stdinPath, "w+") as f:
            f.write(stdin)

    paths['root'] = rootPath
    paths['task'] = os.path.join(taskPath, "task")
    paths['home'] = taskPath

    network = self.installNetwork(task)

    self.writeTask(task, taskPath)
    self.writeNetwork(network, taskPath)

    return (task, paths, network, stdout)

  def finishBuild(self, report, task, taskRevision, originalTaskId):
    # TODO: move this shit somewhere else
    system = self.system.retrieve()

    host = system.host or self.network.hostname()
    port = system.port or 9292

    # Store metadata about the build process
    self.notes.store(task.get('builds').get('id'), 'builds', key=originalTaskId, value={
      "origin": {
        "host": host,
        "port": port
      },
      "time": report['time'],
      "date": report['date'],
      "success": report['code'],
      "backend": task.get('backend')
    }, revision=task.get('builds').get('revision'))

    # Assigns a build id with the id and revision of the task used to build
    self.notes.store(task.get('builds').get('id'),
                     category = 'builds',
                     key      = 'ids',
                     value    = {
                       'id':       originalTaskId,
                       'revision': taskRevision
                     },
                     revision=task.get('builds').get('revision'))

    # Store result (if it isn't local)
    buildPath = report.get('paths').get(task.get('builds').get('index')).get('buildPath')
    self.storage.pushBuild(task.get('builds').get('id'), revision=task.get('builds').get('revision'), buildId=originalTaskId, path=buildPath)

  def execute(self, task, paths, network, stdout, ignoreStdin = False):
    import time
    import datetime

    # Keep track of date
    date = datetime.datetime.utcnow().isoformat()

    taskPath = paths['home']

    # Get stdin stream
    stdin = None
    #if not ignoreStdin:
    #  stdinPath = os.path.join(taskPath, "stdin")
    #  stdin = open(stdinPath, "rb")

    # Run that task (keep track of time)
    elapsed = time.perf_counter()
    exitCode = self.backends.run(task, paths, network, stdout, stdin)
    elapsed = time.perf_counter() - elapsed

    self.cleanUpTaskPath(task, taskPath)

    # Look at the run.json if it exists
    runReport = self.pullRunReport(task, taskPath)

    return {
      'time': elapsed,
      'date': date,
      'code': exitCode,
      'runReport': runReport,
      'paths': paths,
      'id': task.get('id')
    }

  def updateTaskWithRuntimeRequests(self, task, interactive, root = None):
    """ Inserts runtime tags into each provider in the task.

    The interactive parameter indicates that the task wants to run as an
    interactive process.

    The root parameter should be None as it is only used when the function
    recurses.
    """

    if root is None:
      root = task

    if interactive:
      task['interactive'] = interactive

    for subA in task.get('running', []):
      for subB in subA.get('process', []):
        if "provides" in subB and "run" in subB:
          self.updateTaskWithRuntimeRequests(subB, interactive, root)

  def deployBuild(self, task, revision=None, local=False, runSection=None, person=None, uuid = None, interactive=False):
    """ Deploys a build task.
    """

    import datetime
    import json

    originalTaskId = task.get('id')

    if uuid is None:
      from uuid import uuid1
      uuid = str(uuid1())

    task['id'] = uuid

    # TODO: instantiate a writable version of the object for the build
    #       delete that if it isn't local
    # TODO: record the run parameters and build information in the metadata
    taskPath = self.createTaskPath(task)
    self.updateTaskWithRuntimeRequests(task, interactive)

    taskRootPath = os.path.join(taskPath, "task")
    if not os.path.exists(taskRootPath):
      os.mkdir(taskRootPath)

    self.prepareTaskPath(task, taskRootPath, local=local, person=person)

    objectInfo = task.get('builds')
    if local:
      # Local objects get built inside the local directory they are in
      # They get placed in a ".occam-build" path.
      buildPath = os.path.join(os.getcwd(), ".occam-build")
      if not os.path.exists(buildPath):
        os.mkdir(buildPath)

      # Write local copy of the build task
      localTaskPath  = os.path.join(buildPath, "object.json")
      with open(localTaskPath, "w+") as f:
        f.write(json.dumps(task))

      buildPath = os.path.join(buildPath, "build")
      if not os.path.exists(buildPath):
        os.mkdir(buildPath)

      deployInfo = self._deploy(task, taskPath, cwd=os.getcwd(), pathsFor=[
          {'id': objectInfo.get('id'), 'revision': objectInfo.get('revision'), 'path': os.getcwd()}
        ], buildPath=buildPath, runSection = runSection, person = person, interactive=interactive)
    else:
      owner_uuid = self.notes.resolveOwner(objectInfo.get('id'))
      buildPath  = self.storage.buildPathFor(owner_uuid, revision=objectInfo.get('revision'), buildId=originalTaskId, create=True)
      deployInfo = self._deploy(task, taskPath, runSection = runSection, buildPath = buildPath, person = person, interactive=interactive)

    return deployInfo

  def deployRun(self, task, revision=None, arguments = None, command = None, local = False, runSection = None, person = None, stdin = None, stdout = None, uuid = None, interactive=False, cwd=None):
    """ Deploys a run task.
    """

    if uuid is None:
      from uuid import uuid1
      uuid = str(uuid1())

    task['id'] = uuid

    runs_uuid = task.get('runs', {}).get('id')
    runs_revision = task.get('runs', {}).get('revision')

    runningObject = self.getObjectFromTask(task, runs_uuid, runs_revision)

    # Add arguments
    if runningObject is None:
      JobManager.Log.error("Task does not contain the object requested to run.")
      # TODO: raise exception
      return -1

    if command:
      runningObject['run'] = runningObject.get('run', {})
      runningObject['run']['command'] = shlex.split(command)

    if 'run' in runningObject and not arguments is None:
      runningObject['run']['arguments'] = arguments

      if 'command' in runningObject['run']:
        runningObject['run']['command'].extend(arguments)

    taskPath = self.createTaskPath(task)

    if runSection:
      task = self.updateRunSection(task, runSection)

    self.updateTaskWithRuntimeRequests(task, interactive)

    taskRootPath = os.path.join(taskPath, "task")
    if not os.path.exists(taskRootPath):
      os.mkdir(taskRootPath)

    self.prepareTaskPath(task, taskRootPath, person=person)
    if local:
      # If we are building a local object, the built version goes in .occam-build
      # Therefore, mount that path for the running object

      buildPath = os.path.join(os.getcwd(), ".occam-build")
      if not os.path.exists(buildPath):
        buildPath = None
      else:
        buildPath = os.path.join(buildPath, "build")
        if not os.path.exists(buildPath):
          buildPath = None

      ret = self._deploy(task, taskPath, cwd=os.getcwd(), buildPath=buildPath, runSection = runSection, person = person, stdin = stdin, stdout = stdout, interactive=interactive)
    else:
      ret = self._deploy(task, taskPath, cwd=cwd, runSection = runSection, person = person, stdin = stdin, stdout = stdout, interactive=interactive)

    return ret

  def pullRunReport(self, task, taskPath):
    """ Returns the array of run sections found within a run.json if it exists.
    """

    objectIndex = task.get('runs', task.get('builds', {}))['index']

    runReportPath = os.path.join(taskPath, "task", "objects", str(objectIndex), "run.json")

    if not os.path.exists(runReportPath):
      return []

    import json

    ret = []

    with open(runReportPath) as f:
      try:
        ret = json.load(f)
      except:
        ret = []

    return ret

  def _createJob(self, task, revision, person=None, finalize=None, finalizeTag=None, run=None, interactive=False):
    import datetime
    from occam.jobs.records.job import JobRecord

    session = self.database.session()

    job_db = JobRecord()

    job_db.task_backend  = task.get('backend', "")
    job_db.task_name     = task.get('name', "NoName")
    job_db.task_uid      = task.get('id')
    job_db.task_revision = revision

    # The component that requests a callback
    job_db.finalize      = finalize
    job_db.finalize_tag  = finalizeTag

    # Whether or not the job is meant to run as an interactive process
    job_db.interactive   = interactive

    # TODO: handle other schedulers
    job_db.scheduler = "occam"

    if person:
      job_db.person_uid = person.uuid

    job_db.status = "queued"

    if 'builds' in task and 'type' in task and task['type'] == "task":
      job_db.kind = "build"
    elif 'type' in task and task['type'] == "task":
      job_db.kind = "run"
    elif 'type' in task and task['type'] == "workflow":
      job_db.kind = "workflow"

    job_db.queue_time = datetime.datetime.now()

    self.database.update(session, job_db)
    self.database.commit(session)
    if job_db.kind == "workflow" and run is not None:
      self.insertJobRun(job_db, run)

    return job_db

  def launch_daemon(self):
    """ Retrieves an instance of the daemon class.
    """
    from occam.jobs.daemon import JobDaemon
    JobDaemon().start()

  def retrieve(self, status = None, kind = None, task = None, job_id = None):
    """ Retrieves a list of JobRecord items for the given criteria.
    """

    from occam.jobs.records.job import JobRecord

    import sql

    jobs = sql.Table('jobs')
    
    query = jobs.select()
    if status:
      if query.where: 
        query.where = query.where & (jobs.status == status)
      else:
        query.where = (jobs.status == status)

    if kind:
      if query.where: 
        query.where = query.where & (jobs.kind == kind)
      else:
        query.where = (jobs.kind == kind)

    if task:
      if query.where: 
        query.where = query.where & (jobs.task_uid == task)
      else:
        query.where = (jobs.task_uid == task)

    if job_id:
      if query.where: 
        query.where = query.where & (jobs.id == job_id)
      else:
        query.where = (jobs.id == job_id)

    session = self.database.session()
    self.database.execute(session, query)

    rows = self.database.many(session)

    return [JobRecord(x) for x in rows]

  def insertJobRun(self, job, run):
    from occam.jobs.records.job_run import JobRunRecord
    session = self.database.session()
    jobrun = JobRunRecord()
    jobrun.job_id = job.id
    jobrun.run_id = run.id

    # Add to the database
    self.database.update(session, jobrun)
    self.database.commit(session)
    return jobrun


  def pull(self):
    """ Atomically retrieves a job to run.

    This will pull a job while also marking it as 'started'.

    Returns:
      JobRecord: An available Job.
      None: No jobs are left on the queue.
    """

    import sql
    import datetime

    from occam.jobs.records.job import JobRecord

    jobs = sql.Table('jobs')

    session = self.database.session()

    # Find queued jobs with no dependencies
    query = jobs.select()
    query.where = (jobs.status == "queued")

    self.database.execute(session, query)

    rows = self.database.many(session)
    # For each one, attempt to switch the status
    # If successful, then return that job
    for row in rows:
      row = JobRecord(row)
      row.status = "started"
      row.start_time = datetime.datetime.today()

      # Update the job's db row
      query = jobs.update(where   = (jobs.status == "queued") & (jobs.id == row.id),
                          columns = [jobs.status, jobs.start_time],
                          values  = [row.status, row.start_time])

      # Count the number of updated rows (should be 1)
      count = self.database.execute(session, query)
      self.database.commit(session)

      # If count was 0, then it didn't update, otherwise, return the job:
      if count == 1:
        # Successfully updated the job record
        # Return the record
        row.status = "started"
        return row

    # If there are no jobs, then return None
    return None

  def finish(self, job):
    """ Marks the job as complete.
    """

    import datetime

    session = self.database.session()

    job.status = "finished"
    job.finish_time = datetime.datetime.now()

    self.database.update(session, job)
    self.database.commit(session)

  def failure(self, job):
    """ Marks the job as a failure.
    """

    session = self.database.session()

    job.status = "failed"

    self.database.update(session, job)
    self.database.commit(session)


  def runJob(self, job, interactive=False):
    # Get who is running this task
    person = None
    if job.person_uid:
      person = self.objects.retrieve(job.person_uid)
      # # TODO: don't do this
      person.roles = ["administrator"]

    # Get the task itself
    taskObject = self.taskForJob(job, person)

    task = None
    if taskObject:
      task = self.objects.infoFor(taskObject)

    if task is None:
      # Error
      job.status = "failed"
      return -1

    # Deploy each phase
    task, paths, network, stdout = self.deploy(task,
                                               revision    = taskObject.revision,
                                               person      = person,
                                               interactive = interactive)

    # Set the job's path
    self.updatePath(job, paths['task'])

    # Execute the task
    run = self.execute(task, paths, network, stdout)

    # Deploy secondary phases
    for runSection in run['runReport']:
      data = self.deploy(task,
                         revision    = taskObject.revision,
                         runSection  = runSection,
                         person      = person,
                         uuid        = run['id'],
                         interactive = interactive)
      run = self.execute(*data)


  def deploy(self, task, revision, local=False, arguments = None, command = None, person = None, runSection = None, stdin = None, stdout = None, uuid = None, interactive=False):
    """ Deploys the given task.

    Returns:
      int: The exit code of the deployed process.
    """

    # Determine if it is a build or run task.

    if not task.get('builds') is None:
      return self.deployBuild(task, revision=revision, local=local, runSection = runSection, person = person, interactive=interactive)
    elif not task.get('runs') is None:
      return self.deployRun(task, revision=revision, local=local, arguments = arguments, command = command, person = person, runSection = runSection, stdin = stdin, stdout = stdout, uuid = uuid, interactive=interactive)
    else:
      # We don't understand this task
      return [None, None, None, None]

  def kill(self, task):
    """ Kills the given task.
    """

    if task is None:
      return None

    self.backends.kill(task)

  def createTaskPath(self, task):
    """ Creates a path for the running task.
    """

    taskPath = self.createPathFor(task['id'], "runs")

    return taskPath

  def createPathFor(self, uuid, subPath='runs'):
    """ Creates a path to store an object with the given uuid.
    """

    if uuid is None:
      return None

    code = uuid
    code1 = code[0:2]
    code2 = code[2:4]

    path = self.configuration.get('paths', {}).get(subPath, os.path.join(Config.root(), subPath))

    if path is None:
      return None

    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code1)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code2)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, uuid)
    if not os.path.exists(path):
      os.mkdir(path)

    return path

  def cleanUpTaskPath(self, task, path, root=None):
    """
    """

    if root is None:
      root = task

    # Move stdout
    # Look at running task
    runningObject = root.get('runs', root.get('builds'))
    runningTaskLocal = os.path.join(path, "task", "objects", str(runningObject['index']))

    stdoutPath = os.path.join(runningTaskLocal, "stdout")
    stderrPath = os.path.join(runningTaskLocal, "stderr")

    for oldPath in [stdoutPath, stderrPath]:
      if os.path.exists(oldPath):
        i = 0
        while True:
          newPath = oldPath + ".%s" % (str(i))
          i += 1

          if not os.path.exists(newPath):
            break

        os.rename(oldPath, newPath)

  def prepareTaskPath(self, task, path, root=None, build=None, local=False, person=None):
    """
    """

    import json

    if build is None:
      build = 'builds' in task

    if root is None:
      root = task

    # Create a path that represents contents in root
    rootPath = os.path.join(path, "root")
    if not os.path.exists(rootPath):
      os.mkdir(rootPath)

    # Create a path that holds local data for the objects
    objectsPath = os.path.join(path, "objects")
    if not os.path.exists(objectsPath):
      os.mkdir(objectsPath)

    # Create an events file/socket to communicate any actions of objects
    eventsPath = os.path.join(path, "events")
    if not os.path.exists(eventsPath):
      with open(eventsPath, "w+") as f:
        pass

    # For this object, add it's object path
    if 'index' in task and ('run' in task or (build and task['index'] == root['builds']['index'])):
      objectPath = os.path.join(path, "objects", str(task['index']))
      if not os.path.exists(objectPath):
        os.mkdir(objectPath)

      inputsPath  = os.path.join(objectPath, "inputs")
      outputsPath = os.path.join(objectPath, "outputs")

      inputSymlinkPath  = os.path.join(objectPath, "input")
      outputSymlinkPath = os.path.join(objectPath, "output")

      localPath = os.path.join(objectPath, "local")

      # When this is a build task, there is also a place for
      # built version of objects to go.
      if build and task['index'] == root['builds']['index']:
        buildPath = os.path.join(objectPath, "build")

        if not os.path.exists(buildPath):
          os.mkdir(buildPath)

        # Instantiate the object in the local path
        if not local:
          self.storage.instantiate(task['id'], task['revision'], localPath)
          buildObject = self.objects.retrieve(task['id'], revision=task['revision'], person = person)

          self.objects.install(buildObject, localPath, build = True)

      if not os.path.exists(inputsPath):
        os.mkdir(inputsPath)

      if not os.path.exists(outputsPath):
        os.mkdir(outputsPath)

      if not os.path.exists(localPath):
        os.mkdir(localPath)

      for idx, outputInfo in enumerate(task.get('outputs', [])):
        subOutputPath = os.path.join(outputsPath, str(idx))

        if not os.path.exists(subOutputPath):
          os.mkdir(subOutputPath)

        # Allow for at least one output
        subSubOutputPath = os.path.join(subOutputPath, "0")

        if not os.path.exists(subSubOutputPath):
          os.mkdir(subSubOutputPath)

        #for outputIndex, outputInfo in enumerate(wire):
        #  subSubOutputPath = os.path.join(subOutputPath, str(outputIndex))
        #  if not os.path.exists(subSubOutputPath):
        #    os.mkdir(subSubOutputPath)

      for idx, wire in enumerate(task.get('inputs', [])):
        subInputPath = os.path.join(inputsPath, str(idx))

        if not os.path.exists(subInputPath):
          os.mkdir(subInputPath)

        for inputIndex, inputInfo in enumerate(wire.get('connections', [])):
          subSubInputPath = os.path.join(subInputPath, str(inputIndex))

          if not os.path.exists(subSubInputPath):
            os.mkdir(subSubInputPath)

      if not os.path.lexists(inputSymlinkPath):
        os.symlink(os.path.join("inputs", "0", "0"), inputSymlinkPath)

      if not os.path.lexists(outputSymlinkPath):
        os.symlink(os.path.join("outputs", "0", "0"), outputSymlinkPath)

      # TODO: link inputs to output paths when appropriate

      subTaskManifestPath = os.path.join(objectPath, "task.json")

      # Place the task component into this path
      f = open(subTaskManifestPath, "w+")
      json.dump(task, f, indent=2, separators=(',', ': '))
      f.close()

    # For each object, create the directory it runs in
    for wire in task.get('inputs', []) + [{'connections': task.get('running', [{}])[0].get('process', [])}]:
      for obj in wire.get('connections', []):
        self.prepareTaskPath(obj, path, root=root, build=build, local=local, person = person)

  def pathsForTask(self, task, buildObject=None, root=None, create=False, buildPath=None, person=None):
    """ Returns the paths to each object given in a task.
    """

    if root is None:
      root = task

    ret = {}

    for wire in task.get('inputs', []) + [{"connections": task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        if 'index' in inputObject:
          uuid = inputObject.get('id')
          owner_uuid = inputObject.get('owner', {}).get('id', uuid)
          revision = inputObject.get('revision')
          buildId = inputObject.get('buildId')
          obj = self.objects.retrieve(owner_uuid, revision = revision, person = person)
          ownerInfo = self.objects.infoFor(obj)
          built = not inputObject.get('index') == root.get('builds', {}).get('index') and 'build' in ownerInfo

          path = None
          if not obj is None:
            if built:
              # Discover the built version of this object (or build it?? or report that we need to build it??)
              buildIds = self.notes.retrieveBuildIds(owner_uuid, revision)

              # For every build we know, we attempt to find that build's content
              if buildIds:
                for build in buildIds:
                  build = build["id"]
                  path = self.objects.buildPathFor(obj, build)

                  if path is not None:
                    buildId = build
                    break
            elif not buildId is None:
              path = self.objects.buildPathFor(obj, buildId)
            else:
              path = self.objects.instantiate(obj)

          if path is None:
            JobManager.Log.warning("hmm can't find an object %s %s %s" % (owner_uuid, revision, buildId))

          mountPath  = inputObject.get('paths', {}).get('mount')
          mountPaths = inputObject.get('paths', {}).get('volume', {})

          if mountPaths:
            mountPath = mountPaths.get(root.get('provides').get('architecture'), {}).get(root.get('provides').get('environment'), mountPath)

          pathInfo = {
            "id": owner_uuid,
            "revision": revision,
            "index": inputObject.get('index'),
            "mount": mountPath,
            "path": path
          }

          if 'builds' in root:
            # This is a build task.
            if root['builds'].get('index') == inputObject.get('index'):
              localMount  = root.get('builds', {}).get('paths', {}).get('localMount')
              localMounts = root.get('builds', {}).get('paths', {}).get('local')
              if len(localMounts) > 0:
                localMount = localMounts.get(task.get('provides').get('architecture'), {}).get(task.get('provides').get('environment'), localMount)
              localPath = localMount
              localPath = os.path.join(localPath, "build")

              pathInfo["buildMount"] = localPath
              if buildPath is None:
                # Object would have already been discovered?
                # TODO: Assess the truth of the above haha
                buildPath = self.storage.buildPathFor(owner_uuid, revision=revision, buildId=root.get('id'), create=True)

              pathInfo["buildPath"] = buildPath
          elif 'runs' in root:
            # This is a run task
            if root['runs'].get('index') == inputObject.get('index'):
              if not buildPath is None:
                pathInfo["path"] = buildPath

          ret[inputObject.get('index')] = pathInfo

          # Recursively find paths to subobjects
          ret.update(self.pathsForTask(inputObject, buildObject=buildObject, root=root, create=create, buildPath=buildPath, person = person))

    return ret

  def installNetwork(self, task, root = None, output = None):
    """ Instantiates the network knowledge for this object.

    This will allocate ports when necessary.
    """

    if root is None:
      root = task

    if output is None:
      output = {
        "ports": []
      }

    # Look for a network section
    if 'network' in task:
      for port in task['network'].get('ports', []):
        if 'bind' in port:
          JobManager.Log.write("allocating system port 30100 to %s" % (port.get('bind', 0)))
          port['port'] = 30100
          output['ports'].append(port)

    for wire in task.get('inputs', []) + [{'connections': task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        # Recursively install subobjects
        self.installNetwork(inputObject, root, output)

    return output

  def installTask(self, rootPath, task, root=None, paths={}):
    """
    """

    if root is None:
      root = task

    # For this object, add its install paths to root
    if 'init' in task and 'index' in task:
      pathInfo = paths.get(task.get('index'))

      if pathInfo:
        JobManager.Log.noisy("Instantiating %s %s %s@%s" % (task.get('type', 'object'), task.get('name', 'unnamed'), task['id'], task.get('version', task['revision'])))
        # For each install object that is a local path, symlink to its mount path
        if not ("builds" in root and root["builds"]["id"] == task["id"] and root["builds"]["revision"] == task["revision"]):
          mountPath  = task.get('paths', {}).get('mount')
          mountPaths = task.get('paths', {}).get('volume', {})

          if len(mountPaths) > 0:
            mountPath = mountPaths.get(root.get('provides').get('architecture'), {}).get(root.get('provides').get('environment'), mountPath)

          # Link files into the system
          for installInfo in task['init'].get('link', []):
            # TODO: check if this is a resource (it has an 'id' and 'revision')
            # this would be added as a dependency.

            if not 'id' in installInfo:
              installPath = os.path.join(mountPath, installInfo.get('source'))
              installDestination = installInfo.get('to')
              localPath = os.path.join(pathInfo['path'], installInfo.get('source'))

              # Create symlinks for each item in the task
              self.linkTaskDirectory(rootPath, localPath, installPath, installDestination)

          # Copy files into the system
          for installInfo in task['init'].get('copy', []):
            if not 'id' in installInfo:
              installPath = os.path.join(mountPath, installInfo.get('source'))
              installDestination = installInfo.get('to')
              localPath = os.path.join(pathInfo['path'], installInfo.get('source'))

              # Create symlinks for each item in the task
              self.copyTaskDirectory(rootPath, localPath, installPath, installDestination)

    for wire in task.get('inputs', []) + [{'connections': task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        # Recursively find paths to subobjects
        self.installTask(rootPath, inputObject, root, paths)

  def copyTaskDirectory(self, rootPath, localPath, mountPath, destinationPath, recurse=True):
    """ Copies the files from the local path to the given destination.
    """

    # Ensure that the destination path exists
    localDestinationPath = os.path.join(rootPath, destinationPath[1:])

    import errno
    import shutil

    #try:
    if os.path.exists(localPath):
      if os.path.isdir(localPath):
        files = os.listdir(localPath)

        if os.path.lexists(localDestinationPath) and os.path.islink(localDestinationPath):
          os.remove(localDestinationPath)

        if not os.path.exists(localDestinationPath):
          try:
            os.makedirs(localDestinationPath)
          except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(localDestinationPath):
              pass
            else:
              raise

        for filename in files:
          copyDestination = os.path.join(localDestinationPath, filename)
          copySource = os.path.join(localPath, filename)
          if os.path.isdir(copySource):
            if recurse:
              self.copyTaskDirectory(rootPath, copySource, None, os.path.join(destinationPath, filename))
          else:
            if os.path.lexists(copyDestination):
              os.remove(copyDestination)
            shutil.copy(copySource, copyDestination)
      else:
        copyDestination = localDestinationPath
        copySource = localPath

        try:
          os.makedirs(os.path.dirname(localDestinationPath))
        except FileExistsError:
          pass

        shutil.copy(copySource, copyDestination)
    #except FileNotFoundError as e:
    #  JobManager.Log.error("Could not link files within the object to the VM filesystem: %s" % (e.strerror))
    #  return None

  def linkTaskDirectory(self, rootPath, localPath, mountPath, destinationPath, recurse=True):
    """ Creates symlinks at destination that point to the mounted paths by walking the
    localPath.
    
    The localPath gives a local view on what will be seen inside the
    virtual machine. We can duplicate that structure and point them to each file
    with a path relative to the virtual machine (mountPath)
    """

    # Ensure that the destination path exists
    localDestinationPath = os.path.join(rootPath, destinationPath[1:])

    import errno

    #try:
    if os.path.exists(localPath):
      if not os.path.islink(localPath) and os.path.isdir(localPath):
        files = os.listdir(localPath)

        if os.path.lexists(localDestinationPath) and os.path.islink(localDestinationPath):
          os.remove(localDestinationPath)

        if not os.path.exists(localDestinationPath):
          try:
            os.makedirs(localDestinationPath)
          except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(localDestinationPath):
              pass
            else:
              raise

        for filename in files:
          localSymlink = os.path.join(localDestinationPath, filename)
          localFilePath = os.path.join(localPath, filename)

          # We always replace the symlink with a new one
          if os.path.lexists(localSymlink) and os.path.islink(localSymlink):
            os.remove(localSymlink)

          # If the file to copy in is a directory, then override any link
          # and then recurse into that directory
          if not os.path.islink(localFilePath) and os.path.isdir(localFilePath):
            # TODO: intelligently detect when we can just symlink the directory
            if recurse:
              self.linkTaskDirectory(rootPath, os.path.join(localPath, filename), os.path.join(mountPath, filename), os.path.join(destinationPath, filename))
          else:
            destination = os.path.join(mountPath, filename)

            # We won't replace a directory with a symlink
            if not os.path.exists(localSymlink):
              os.symlink(destination, localSymlink)
      else:
        destination  = mountPath
        localSymlink = localDestinationPath

        try:
          os.makedirs(os.path.dirname(localDestinationPath))
        except FileExistsError:
          pass

        if not os.path.lexists(localSymlink):
          os.symlink(destination, localSymlink)
    #except FileNotFoundError as e:
    #  JobManager.Log.error("Could not link files within the object to the VM filesystem: %s" % (e.strerror))
    #  return None

  def getObjectFromTask(self, task, uuid, revision):
    """ Finds the first instance of the given object in the task.
    """

    for obj in task.get('inputs', []) + task.get('running', [{}])[0].get('process', []):
      if obj.get('id') == uuid and obj.get('revision') == revision:
        return obj
      else:
        ret = self.getObjectFromTask(obj, uuid, revision)
        if not ret is None:
          return ret

    return None

  def jobFromId(self, job_or_job_id):

    if isinstance(job_or_job_id, int) or isinstance(job_or_job_id, str):
      jobs = self.retrieve(job_id = job_or_job_id)

      job = None
      if len(jobs) != 0:
        return jobs[0]
    else:
      return job_or_job_id

    return None

  def taskForJob(self, job_or_job_id, person = None):
    """ Returns the task for the given job as the given Person.
    """

    job = self.jobFromId(job_or_job_id)

    if job is None:
      return None

    # Get the task itself
    uuid       = job.task_uid
    revision   = job.task_revision
    return self.objects.retrieve(uuid, revision=revision, person=person)

  def networkInfoFileForJob(self, job_or_job_id):
    """ Returns the stream for the network manifest for the given job, if it exists.
    """

    job = self.jobFromId(job_or_job_id)

    if job is None:
      return None

    if not job.path:
      return None

    networkInfoPath = os.path.join(job.path, "..", "network.json")

    if not os.path.exists(networkInfoPath):
      return None

    return open(networkInfoPath, "rb")

  def networkInfoForJob(self, job_or_job_id):
    """ Returns the instantiated network manifest for the running job, if it exists.
    """

    import json

    f = self.networkInfoFileForJob(job_or_job_id)

    networkInfo = {}

    try:
      networkInfo = json.load(f)
    except:
      pass

    return networkInfo

  def taskInfoFileForJob(self, job_or_job_id):
    """ Returns the stream for the task manifest for the given job, if it exists.
    """

    job = self.jobFromId(job_or_job_id)

    if job is None:
      return None

    if not job.path:
      return None

    taskObjectPath = os.path.join(job.path, "object.json")

    if not os.path.exists(taskObjectPath):
      return None

    return open(taskObjectPath, "rb")

  def taskInfoForJob(self, job_or_job_id):
    """ Returns the instantiated task for the running job, if it exists.
    """

    import json

    f = self.taskInfoFileForJob(job_or_job_id)

    taskInfo = {}

    try:
      taskInfo = json.load(f)
    except:
      pass

    return taskInfo

  def taskIdForJob(self, job_or_job_id):
    """ Returns the instantiated id for the running job, if it exists.
    """

    taskInfo = self.taskInfoForJob(job_or_job_id)

    if taskInfo is None:
      return None

    return taskInfo.get('id')

  def pidForJob(self, job_id):
    """ Returns the process id for the given job, if it is running.

    Returns:
      process_id (int): The process id for the job runner or None if the job is
                        not running.
    """

    metadata = self.metadataFor(job_id)

    if metadata is None:
      return None

    # Get the running job's process id
    pid = metadata.get("pid")

    # Get the command name for the given process
    cmd_check = None
    try:
      with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
        cmd_check = f.read()
    except:
      pass

    # We use endswith since it may append the full path to the initial process
    # name, which we may not know
    if cmd_check is None or metadata is None or not metadata.get("check", "").endswith(cmd_check):
      return None

    return pid

  def jobRunning(self, job_id):
    """ Returns True when the given job is current running.
    """

    # Check the metadata file for the pid and cmd name
    # Check for the presence of the pid with the cmd name
    # Return True if that checks out

    return self.pidForJob(job_id) is not None

  def updatePath(self, job, path):
    """ Updates the path of the given job.
    """

    session = self.database.session()

    job.path = path

    self.database.update(session, job)
    self.database.commit(session)

    return job

  def metadataFor(self, job_id):
    """ Returns the metadata for the given job.
    """

    import json
    from occam.config import Config

    basePath = Config.root()

    try:
      ret = json.loads(open(os.path.join(basePath, "job-daemon", "%s.json" % (job_id)), "r").read())
    except:
      ret = None

    return ret

  def eventLogFor(self, job_id):
    rows = self.retrieve(job_id = job_id)
    if len(rows) == 0:
      return None

    job = rows[0]
    if not job.path:
      return None

    eventPath = os.path.join(job.path, "events")
    if not os.path.exists(eventPath):
      return None

    return open(eventPath, "rb")

  def writeTo(self, job_id, data, person):
    """ Writes the given data to the given job.
    """

    pid = self.pidForJob(job_id)

    if not pid:
      return None

    with open("/proc/%s/fd/0" % (pid), "wb") as f:
      f.write(data)

  def logFor(self, job_id):
    from occam.config import Config

    basePath = Config.root()

    try:
      ret = open(os.path.join(basePath, "job-daemon", "%s.log" % (job_id)), "rb")
    except:
      ret = None

    return ret

  def rakeOutput(self, task, taskPath, generators = None, root = None, person=None):
    """ Given a run report produced by a call to deploy, gather the generated output.
    """
    # TODO: move this method out of the job manager

    # Look at the possible outputs

    if root is None:
      root = task

    outputs = []

    for wire in task.get('inputs', []) + [{"connections": task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        if 'index' in inputObject:
          outputs.extend(self.rakeOutput(inputObject, taskPath, generators=generators, root=root))


    if 'index' in task and 'outputs' in task:
      objectPath = os.path.join(taskPath, "objects", str(task['index']))
      for wireIndex, wire in enumerate(task.get('outputs', [])):
        outputPath = os.path.join(objectPath, "outputs", str(wireIndex))

        # Create DB entry for output of the node pin
        new_output={}
        new_output["output_index"] = wireIndex
        new_output["output_count"] = 0
        new_output["objects"]=[]

        # Look for the existence of any new outputs
        # Remember, the first one is assumed, but if it is empty, ignore
        # Ignore all empty outputs
        i = 0
        while os.path.exists(os.path.join(outputPath, str(i))):
          subOutputPath = os.path.join(outputPath, str(i))
          i += 1

          # Is this directory empty?
          if len(os.listdir(subOutputPath)) == 0:
            continue

          # Rake this object
          outputInfo = wire.copy()

          subOutputJSON = os.path.join(subOutputPath, "object.json")
          if os.path.exists(subOutputJSON):
            definedInfo = {}
            try:
              with open(subOutputJSON, "r") as f:
                import json
                definedInfo = json.load(f)
            except:
              JobManager.Log.warning("Could not open the object.json of the next output.")
              definedInfo = {}

            outputInfo.update(definedInfo)

          # Set defaults
          outputInfo['type'] = outputInfo.get('type', 'object')
          outputInfo['name'] = outputInfo.get('name', 'output')

          # Handle schema
          if 'schema' in wire and outputInfo['schema'] == wire['schema'] and isinstance(wire['schema'], str):
            outputInfo['schema'] = {
              "id": task['id'],
              "name": wire.get('name'),
              "type": "application/json",
              "revision": task['revision'],
              "file": wire['schema']
            }

          # Add generator relation (The generator is the object, and any passed along)
          outputInfo['generator'] = [{
            "name": task.get('name'),
            "type": task.get('type'),
            "id":   task.get('id'),
            "revision": task.get('revision')
          }]

          for generator in (generators or []):
            outputInfo['generator'].append(generator)

          # Add 
          JobManager.Log.write("Raking %s %s" % (outputInfo['type'], outputInfo['name']))

          outputObject = self.objects.write.create(name        = outputInfo['name'],
                                                   object_type = outputInfo['type'],
                                                   subtype     = outputInfo.get('subtype'),
                                                   path        = subOutputPath,
                                                   info        = outputInfo,
                                                   createPath  = False)
          self.objects.write.store(outputObject)

          # Add note for each generator
          for generator in outputInfo['generator']:
            self.addObjectToOuputs(outputObject, generator)

          new_output["output_count"] += 1
          out_object = {}
          out_object["output_index"] = wireIndex
          out_object["object_uid"] = outputObject.uuid
          out_object["object_revision"] = outputObject.revision
          new_output["objects"].append(out_object)
        outputs.append(new_output)
    return outputs

  def addObjectToOuputs(self, object, recipient):
    objectInfo = self.objects.infoFor(object)
    self.notes.store(
      recipient.get('id'),
      'outputs',
      key="object",
      value={
      "name":     objectInfo['name'],
      "type":     objectInfo['type'],
      "id":       object.uuid,
      "revision": object.revision,
      },
      revision=recipient.get('revision'),
      append=True)
    return

def scheduler(name: str):

  """ This decorator will register a possible scheduler backend.
  """

  def _scheduler(cls):
    JobManager.register(name, cls)
    return cls

  return _scheduler
