# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e (defaults to object.json)
# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e/object.json (same as default)
# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e/configs/0/input.json
# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e@abc124/configs/0/input.json

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.key_parser import KeyParser

from occam.manager import uses

from occam.objects.manager  import ObjectManager
from occam.discover.manager import DiscoverManager

@command('objects', 'view',
  category      = 'Object Inspection',
  documentation = "Displays the metadata for the object.")
@argument("objects", type = "object", nargs = '*', help = "A list of object identifiers of the form <uuid>@<revision>/<path>. The default path is '/object.json'")
@option("-a", "--all",  dest   = "list_all",
                        action = "store_true",
                        help   = "includes all files within resources as well and will redirect to linked resources")
@option("-k", "--key",  action = "store",
                        dest   = "key",
                        help   = "the key whose value we'll pull from the object metadata or JSON file")
@option("-i", "--info", action = "store_true",
                        dest   = "retrieve_info",
                        help   = "when specified, will return the realized object info for the object, ignoring path and owner.")
@uses(ObjectManager)
@uses(DiscoverManager)
class View:
  def printObject(self, obj, path):
    fromObject = False
    if path == "":
      path = "/object.json"
      fromObject = True

    if self.options.key:
      try:
        if self.options.retrieve_info:
          data = self.objects.infoFor(obj)
        else:
          data = self.objects.retrieveJSONFrom(obj, path)
        if data is None and object:
          # Attempt to discover
          self.objects.discover(uuid=object.id, revision=object.revision)
          data = self.objects.retrieveJSONFrom(obj, path, fromObject=fromObject)
      except ValueError:
        Log.error("Could not parse JSON data.")
        return -1
      except IOError:
        Log.error("Could not locate data.")
        return -1

      parser = KeyParser()
      try:
        value = parser.get(data, self.options.key)
      except:
        Log.error("Key could not be found in document")
        return -1

      value = json.dumps(value)
      Log.output(value, padding="")
    else:
      #try:
      if self.options.retrieve_info:
        data = json.dumps(self.objects.infoFor(obj))
        Log.output(data, padding="")
        return
      else:
        data = self.objects.retrieveFileFrom(obj, path, includeResources=self.options.list_all, person=self.person, fromObject=fromObject)
      #except:
      #  Log.error("Could not locate data.")
      #  return -1

      if data is None or object is None:
        Log.error("No object found in the current directory.")
        return -1

      Log.pipe(data)

  def do(self):
    obj = None
    path = ""

    if self.options.objects is None:
      # Use the object in the current directory if <object> argument is omitted
      obj, _ = self.objects.retrieveLocal(".")

      self.printObject(obj, path)
      return 0
    else:
      for object in self.options.objects:
        # Query for object by id
        if object is None:
          Log.error("the object given id is invalid")
          return -1
        obj = self.discover.resolve(object, person=self.person)
        if obj is None:
          Log.error("cannot find object with id %s" % (object.id))
          return -1

        if object.path:
          path = object.path

        self.printObject(obj, path)
        return 0
