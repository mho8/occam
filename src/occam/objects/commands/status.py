# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.objects.manager     import ObjectManager
from occam.databases.manager   import DatabaseManager
from occam.permissions.manager import PermissionManager
from occam.accounts.manager    import AccountManager

from occam.manager import uses

@command('objects', 'status',
  category      = 'Object Inspection',
  documentation = "Lists high-level information about the given object or current path.")
@argument("object", type = "object", nargs = '?')
@option("-a", "--all",  dest   = "list_all",
                        action = "store_true",
                        help   = "includes all files within resources as well and will redirect to linked resources")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)     # For pulling the object
@uses(DatabaseManager)   # For pulling out the normalized data
@uses(PermissionManager) # For pulling access control information
@uses(AccountManager)    # For determining authorship
class StatusCommand:
  """ This class handles listing the current knowledge about an object or a file without an object.

  For local paths when developing an object, this will show you the differences
  between the stored object and the current state of the object.
  """

  def do(self):
    # Get the object to list
    obj = None
    path = "/"

    if self.options.object is None:
      # Use the object in the current directory if <object> argument is omitted
      from occam.object import Object

      obj = Object(path='.')
      ret = {}
    elif self.options.object.id:
      # Query for object by id
      obj = self.objects.resolve(self.options.object, person = self.person)
      if obj is None:
        Log.error("cannot find object with id %s" % (self.options.object.id))
        return -1

      if self.options.object.path:
        # Return file stat
        ret = self.objects.retrieveFileStatFrom(obj, path             = self.options.object.path,
                                                     includeResources = self.options.list_all,
                                                     person           = self.person)
      else:
        # Get the database knowledge of the object
        db_rows = self.objects.search(uuid=obj.uuid)
        storedRevision = None
        if db_rows:
          db_obj = db_rows[0]
          storedRevision = db_obj.revision

        person_uuid = None
        if self.person:
          person_uuid = self.person.uuid

        # Gather access permissions
        mainAccess = self.permissions.retrieve(
            uuid        = obj.uuid,
            person_uuid = person_uuid)

        records = self.permissions.retrieveAllRecords(
            uuid        = obj.uuid, 
            person_uuid = person_uuid)

        # Determine each type of access
        universeAccess = {}
        personAccess   = {}

        universeChildAccess = {}
        personChildAccess   = {}

        # Put into a more standard form:
        for record, person in records:
          item = {}
          if record.can_read is not None:
            item['read']  = record.can_read  == 1
          if record.can_write is not None:
            item['write'] = record.can_write == 1
          if record.can_clone is not None:
            item['clone'] = record.can_clone == 1
          if record.can_run is not None:
            item['run'] = record.can_run == 1

          if record.person_object_id is None and record.for_child_access == 1:
            universeChildAccess = item
          if record.person_object_id is None and record.for_child_access == 0:
            universeAccess = item
          if record.person_object_id is not None and record.for_child_access == 1:
            personChildAccess = item
          if record.person_object_id is not None and record.for_child_access == 0:
            personAccess = item

        # Get authors/collaborators
        authors = self.accounts.authorsFor(obj.uuid)
        collaborators = self.accounts.collaboratorsFor(obj.uuid)

        # Generate the result
        ret = {
          "id": obj.uuid,
          "revision": obj.revision,
          "currentRevision": storedRevision,
          "access": {
            "current": mainAccess,
            "object": {
              "person":   personAccess,
              "universe": universeAccess
            },
            "children": {
              "person":   personChildAccess,
              "universe": universeChildAccess
            }
          },
          "authors": [x._data for x in authors],
          "collaborators": [x._data for x in collaborators]
        }

        # If this is a Person, determine authorization credentials
        if db_obj.object_type == "person":
          db_account = self.accounts.retrieveAccount(person_uuid = db_obj.uid)
          if db_account:
            ret["account"] = {
              "username": db_account.username,
              "roles":    (db_account.roles or ";;").split(';')[1:-1]
            }

            if db_account.email and db_account.email_public:
              ret["account"]["email"] = db_account.email

        # Handle index navigation (aka uuid@revision[1][2][0]
        #   will report "roots" with 3 objects, the root, the item at root[1]
        #   and the object at root[1][2])
        if obj.roots:
          ret["roots"] = []

          for subObject in obj.roots or []:
            subInfo = self.objects.infoFor(subObject)
            ret["roots"].append({
              "id": subInfo["id"],
              "type": subInfo["type"],
              "name": subInfo["name"],
              "revision": subObject.revision})

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      Log.output(ret)

    return 0
