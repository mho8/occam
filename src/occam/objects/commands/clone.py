# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.write_manager import ObjectWriteManager
from occam.permissions.manager   import PermissionManager

@command('objects', 'clone',
  category      = 'Object Management',
  documentation = "Creates a copy of the given object.")
@argument("object", type = "object", help = "the object to clone")
@argument("path",   type = str,      help = "path to clone the object", nargs = "?")
@option("-t", "--to",       action = "store",
                            dest   = "to_object",
                            type   = "object",
                            help   = "the object uuid to build")
@option("-n", "--name",     action = "store",
                            dest   = "name",
                            help   = "the new name for the cloned object")
@option("-p", "--temporary",action = "store_true",
                            dest   = "temporary",
                            help   = "create a temporary path (you are responsible for deleting)")
@option("-i", "--internal", action = "store_true",
                            dest   = "store",
                            help   = "build the clone in the object store instead of making a local path")
@option("-l", "--linked",   action = "store_true",
                            dest   = "linked",
                            help   = "whether or not to link to the original object instead of creating a new object with a new uuid")
@option("-j", "--json",     dest    = "to_json",
                            action  = "store_true",
                            help    = "returns result as a json document")
@uses(ObjectWriteManager)
@uses(PermissionManager)
class CloneCommand:
  def do(self):
    Log.header("Cloning object")

    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("Object could not be located")
      return -1

    to_object = None
    if self.options.to_object:
      to_object = self.objects.resolve(self.options.to_object, person = self.person)

      if to_object is None:
        Log.error('Object specified by `--to` could not be located')
        return -1

    obj_type = self.objects.infoFor(obj).get('type', 'object')
    obj_name = self.objects.infoFor(obj).get('name', 'object')

    path = self.options.path
    if path is None or path == "":
      path = os.path.join('.', Object.slugFor(obj_type) + '-' + Object.slugFor(obj_name))

    path = os.path.realpath(path)

    # Clone
    new_obj  = None
    if self.options.store:
      # Clone the object within the provided object
      # We need the space to do this, so make a temp clone of to_object
      new_obj = self.objects.write.clone(obj, to = to_object, person = self.person, info = {
        "name": self.options.name or obj_name
      })
    else:
      Log.write("Cloning to %s" % (path))
      new_obj = self.objects.clone(obj, path = path)

    if new_obj is None:
      Log.error("could not clone")
      return -1

    # Set initial permissions
    self.permissions.update(new_obj.uuid, canRead=False, canWrite=False, canClone=False, canRun=False)

    if self.person:
      self.permissions.update(new_obj.uuid, person_uuid=self.person.uuid, canRead=True, canWrite=True, canClone=True, canRun = True)


    ret = {}
    ret["updated"] = []

    for x in (new_obj.roots or []):
      ret["updated"].append({
        "id": x.uuid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": new_obj.uuid,
      "revision": new_obj.revision,
      "position": new_obj.position,
    })

    if self.options.to_json:
      import json

      Log.output(json.dumps(ret))
    else:
      Log.write("new object id: ", end="")
      Log.output("%s" % (self.objects.infoFor(new_obj)['id']), padding="")
      Log.write("new object revision: ", end="")
      Log.output("%s" % (new_obj.revision), padding="")

    Log.done("Cloned %s %s" % (obj_type, obj_name))
    return 0
