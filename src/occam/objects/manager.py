# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json

from occam.git_repository import GitRepository
from occam.log            import loggable

from occam.object     import Object
from occam.group      import Group
from occam.experiment import Experiment
from occam.workset    import Workset
from occam.person     import Person

from occam.manager import manager, uses

from occam.object_info import ObjectInfo

from occam.storage.manager     import StorageManager
from occam.notes.manager       import NoteManager
from occam.resources.manager   import ResourceManager
from occam.caches.manager      import CacheManager
from occam.links.manager       import LinkManager
from occam.permissions.manager import PermissionManager

from occam.config import Config

@loggable
@manager("objects")
@uses(StorageManager)
@uses(NoteManager)
@uses(ResourceManager)
@uses(CacheManager)
@uses(PermissionManager)
@uses(LinkManager)
class ObjectManager:
  """ This OCCAM manager handles the Object listing, and Object lookup.
  """

  def __init__(self):
    """
    Initialize the object manager.
    """

  def retrieveLocal(self, path, roots=None):
    """ Retrieves the object and its workset from the given path.
    """

    if not os.path.exists(os.path.join(path, "object.json")):
      return None, None

    obj = Object(path = path, roots = roots)

    workset = None

    objectType = self.infoFor(obj).get('type')

    if objectType == "workset":
      obj = Workset(path=path)
      workset = obj
    elif objectType == "group":
      obj = Group(path=path)
    elif objectType == "experiment":
      obj = Experiment(path=path)
    elif objectType == "person":
      obj = Person(path=path)

    if workset is None:
      parentPath = os.path.realpath(os.path.join(path, ".."))
      workset = self.localSearch(parentPath, "workset")
      obj.root = workset

    return obj, workset

  def localSearch(self, startingPath, searchType=None, maxDepth=3):
    """
    Retrieves the object by looking for it in the given path and failing to
    find it, look in the parent directory. If will recurse by the given
    maxDepth. If it cannot find an Object, it will return None.
    """

    if maxDepth < 1:
      return None

    # Look for an Object in the current path
    obj, workset = self.retrieveLocal(startingPath)
    if obj:
      if (searchType is None) or (self.infoFor(obj).get('type') == searchType):
        return obj

    parentPath = os.path.realpath(os.path.join(startingPath, ".."))
    return self.localSearch(parentPath, searchType, maxDepth=(maxDepth-1))

  def resolve(self, option, person = None, allowNone = False):
    """ Returns an Object based on a CLI object argument.
    """

    if option is None and not allowNone:
      return None

    if option is None or option.id == ".":
      # Look in the current directory
      return self.retrieveLocal(".")[0]

    uuid = option.id
    if option.id == "+":
      # Look at the current directory, but resolve globally
      tmpObject = self.retrieveLocal(".")[0]
      uuid = self.infoFor(tmpObject).get("id")

    return self.retrieve(uuid           = uuid,
                         revision       = option.revision,
                         version        = option.version,
                         index          = option.index,
                         link           = option.link,
                         person         = person)

  def retrieve(self, uuid, object_type="object", revision=None, version=None, owner_uuid=None, index=[], file=None, person=None, roots=None, link = None, position=None, penalties=None, object_options = None):
    """ Returns an Object representing the given stored OCCAM object.

    Returns an object instance representing the given stored OCCAM object. Will
    return a Group, Experiment, Workset, or generic Object respective of the
    object type.

    If a person_uuid is given, it will only retrieve the object if the object
    is accessible (readable) by that Person. Otherwise, it will return None
    even if the object is stored.
    """

    if object_options:
      revision = object_options.revision
      version  = object_options.version
      index    = object_options.index
      link     = object_options.link

    if owner_uuid is None:
      owner_uuid = self.notes.resolveOwner(uuid)

    # Determine if the given person has administrator (aka full) access
    administrator = False
    person_uuid   = None
    if person:
      administrator = 'administrator' in person.roles
      person_uuid   = person.uuid

    # Get the access control
    if not administrator:
      accessRecord = self.permissions.retrieve(uuid=owner_uuid, revision=revision, person_uuid=person_uuid)

      # Return None if the object is inaccessible
      if not accessRecord['read']:
        return None

    resolvedVersion = None
    if not version is None:
      ObjectManager.Log.noisy("Looking for version %s of %s" % (version, owner_uuid))
      revision, resolvedVersion = self.notes.resolveVersion(owner_uuid, version, penalties = penalties)

      if revision is None:
        # Cannot find the version requested
        return None

      ObjectManager.Log.noisy("Found revision %s" % (revision))

    if revision is None:
      rows = self.datastore.retrieveObjects(uuid=uuid)
      db_obj = None
      if len(rows) > 0:
        db_obj = rows[0]

      if db_obj is None:
        return None

      revision = db_obj.revision

    if owner_uuid == uuid:
      # Check for owner uuid
      values = self.notes.retrieve(uuid, category="owner", key="id", revision=revision)

      if values:
        for value in values:
          owner_uuid = value['value']
          break

    path = self.storage.repositoryPathFor(owner_uuid, revision=revision)
    localPath = path

    resourcePath = self.storage.resourcePathFor(owner_uuid)

    resource = None

    if resourcePath:
      # This object stores a resource
      resource = self.resources.retrievePath(owner_uuid, resourcePath, revision)

    if not path:
      path = self.pathFor(owner_uuid, subPath="notes")
      localPath = None

    ret = None
    if os.path.exists(path):
      if object_type == 'group':
        ret = Group(localPath, uuid=uuid, revision=revision)
      elif object_type == 'experiment':
        ret = Experiment(localPath, uuid=uuid, revision=revision)
      elif object_type == 'workset':
        ret = Workset(localPath, uuid=uuid, revision=revision)
      else:
        if resource:
          ObjectManager.Log.noisy("Found resource object %s at %s" % (uuid, revision))
        else:
          ObjectManager.Log.noisy("Found object %s at %s" % (uuid, revision))
        ret = Object(path=localPath, uuid=uuid, revision=revision, file=file, resource = resource, roots = roots, link = link, position = position, version=resolvedVersion)

    # Object not found
    if ret is None:
      return None

    if ret and index:
      # Traverse index
      info = self.infoFor(ret)
      if info is None:
        return None
      if index[0] >= 0 and index[0] < len(info.get('contains', [])):
        subInfo = info['contains'][index[0]]
        if roots is None:
          roots = []
        roots = roots[:]
        roots.append(ret)
        ret = self.retrieve(uuid = subInfo.get('id'), revision = subInfo.get('revision'), index = index[1:], person=person, roots = roots, link = link, position = index[0])
      else:
        ret = None

    return ret

  def buildPathFor(self, obj, buildId):
    """
    """

    return self.storage.pathFor(obj.uuid, revision = obj.revision, buildId = buildId)

  def instantiate(self, obj):
    """ This will create a repository in the local store that reflects the given
    revision.
    
    Obviously, this is just a cache of the information in the git
    repository path (<uuid>/repository). It places this in a directory based
    on the revision (<uuid>/<revision>).

    Returns the path to the instantiated object.
    """

    cachePath = self.storage.pathFor(obj.uuid, obj.revision, create=False)

    if cachePath is None:
      cachePath = self.storage.pathFor(obj.uuid, obj.revision, create=True)

      repositoryPath = self.storage.repositoryPathFor(obj.uuid, obj.revision)

      # Clone the object at the requested revision
      git = GitRepository(repositoryPath)

      git.clone(cachePath)
      git.reset(obj.revision, hard = True)

      # Unpack dependencies
      self.install(obj, cachePath)

    return cachePath

  def pathFor(self, uuid, subPath='objects'):
    """ Returns the path the object of the given uuid would be stored.
    """

    code = uuid
    code1 = code[0:2]
    code2 = code[2:4]

    # Objects are in: .occam/objects/<type>/<code[0:2]>/<code[2:4]>/full-code
    object_path = self.configuration.get('paths', {}).get(subPath, os.path.join(Config.root(), subPath)) #self.occam.configuration()['paths'].get(subPath)
    if object_path is None:
      return None
    return os.path.realpath(os.path.join(object_path, code1, code2, uuid))

  def temporaryClone(self, obj, person=None):
    """ Clones the object to a temporary location where it can be updated.
    """

    # We are adding this to an object that already exists
    # We need to create a temporary object to house it

    if obj is None:
      return None, None, None

    if obj.root:
      root = obj.root
    else:
      root = obj

    rootObjectInfo = self.infoFor(root)
    destObjectInfo = self.infoFor(obj)

    dest_obj_name = destObjectInfo.get('name', 'unknown')
    dest_obj_type = destObjectInfo.get('type', 'object')

    # Look for a LocalLink, because this will already be the clone path
    link = obj.link

    if link:
      # The root is already at this path... confirm it
      linkRecords = self.links.retrieveLocalLinks(root, link_id = int(link))
      if len(linkRecords) == 0:
        link = None
      else:
        link = linkRecords[0]
        ObjectManager.Log.write("found link at %s" % (link.path))

    roots = []
    root.roots = roots

    clonedRoot = None

    # Pull workset and all objects at this revision
    if link:
      localRoot, _ = self.retrieveLocal(link.path, roots=roots)
      if localRoot.revision == root.revision:
        clonedRoot = localRoot

    if clonedRoot is None:
      # Clone root to a temporary path
      ObjectManager.Log.write("cloning a temporary copy")
      localRoot = self.clone(root, path=None)
      clonedRoot = localRoot

    root = clonedRoot
    baseRoots = obj.roots or []
    baseRoots = baseRoots[1:]
    if len(obj.roots or []) > 0:
      baseRoots.append(obj)
    obj = root

    # Clone all intermediate objects, when needed
    current = root
    clonePath = root.path
    for baseObject in baseRoots:
      info = self.infoFor(current)
      position = baseObject.position

      clonePath = os.path.join(clonePath, "dependency-%s" % (position))

      roots = roots[:]
      roots.append(current)

      if os.path.exists(clonePath):
        current, _ = self.retrieveLocal(clonePath, roots=roots)

        # Update current to the requested revision
        self.reset(current, baseObject.revision)
        continue

      if len(info.get('contains', [])) > position:
        dependency = info["contains"][position]
        current = self.retrieve(dependency.get('id'), revision = dependency.get('revision'), roots=roots, person = person, position=position)

        if current is None:
          break

        ObjectManager.Log.write("cloning %s to %s" % (dependency.get('id'), clonePath))
        current = self.clone(current, clonePath)

        if not current:
          break

    if not current:
      return None, None, None

    obj = current

    return root, obj, root.path

  def reset(self, obj, revision=None):
    """ Updates the given object that is at a local path to the given revision.

    This will update the object as it is seen in the filesystem to the given revision.
    """

    GitRepository(obj.path).reset(revision=revision, hard=True)
    obj.revision = revision

    return obj

  def clone(self, obj, path=None, increment=True):
    """ Clones the given object to the given path.

    Explicitly clones a copy of this object to the given path. If create
    is True, we assign a new UUID to this object and mark it as cloned.
    Otherwise, we just pull down a copy of the object. Changes will be
    merged into the object itself.

    If path is not given, it will be placed in a temporary path. You should
    delete that path when you are finished with the object.

    If person is given, then the clone will only happen when the given
    Person is allowed. Otherwise, the clone does not happen and None is
    returned.
    """

    temporary = False

    if path is None:
      # Create temporary path
      import tempfile
      path = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
      temporary = True
    else:
      # Ensure it is an absolute path
      path = os.path.realpath(path)

    revision = obj.revision

    if increment and os.path.exists(path):
      path = path + '-1'
      index = 1
      while os.path.exists(path):
        index += 1
        path = path[0:path.rindex('-')] + '-' + str(index)

    if os.path.exists(os.path.join(path, '.git')):
      return False

    ObjectManager.Log.noisy("cloning object to %s" % (path))

    if obj.path:
      GitRepository(obj.path).clone(path)
      git = GitRepository(path)
      if not revision is None:
        git.reset(revision)
    else:
      self.storage.clone(obj.uuid, revision, path=path)

    obj = Object(path=path, uuid=obj.uuid, revision=revision, roots=obj.roots, position = obj.position, link = obj.link)
    obj.temporary = temporary

    return obj

  def parentFor(self, obj, revision=None):
    # "roots" override the hierarchy
    if obj.roots is not None:
      if len(obj.roots) > 0:
        return obj.roots[-1]
      else:
        return None

    # Otherwise, we determine the parent by either the directory structure
    # or the "belongsTo" field
    objectInfo = self.infoFor(obj)
    if 'belongsTo' in objectInfo:
      belongsToUUID = objectInfo['belongsTo']['id']
      if os.path.exists(os.path.join(obj.path, '..', 'object.json')):
        parentObject = Object(path=os.path.join(obj.path, '..'), revision=revision)
        if parentObject and self.infoFor(parentObject)['id'] == belongsToUUID:
          return parentObject

      db_rows = self.datastore.retrieveObjects(uuid=belongsToUUID)
      if db_rows:
        db_obj = db_rows[0]
      else:
        return None

      return self.retrieve(db_obj.uid, db_obj.object_type, revision=revision)
    else:
      if os.path.exists(os.path.join(obj.path, '..', 'object.json')):
        return Object(path=os.path.join(obj.path, '..'))
      return None

  def retrieveHistoryFor(self, obj):
    """ Returns a list of entries for each revision from the current revision.

    Generally, this gives you a timeline for the object.
    """

    revision = obj.revision

    if obj.path:
      try:
        return GitRepository(obj.path, revision=revision).history()
      except IOError:
        pass

    return self.storage.retrieveHistory(obj.uuid, revision)

  def retrieveDirectoryWithResourcesFrom(self, obj, path, person, buildId = None):
    """ Yields a list of tuples regarding files or directories within resources of the given object and subpath.

    The result is an array of tuples containing:
    (subObj: the resource Object, data: directory listing, internalPath: the path within the resource).

    When internalPath is None, this is not a file within the resource. Rather,
    it is the resource itself where it would be installed in the given Object.
    """

    ret = []

    info = self.infoFor(obj)

    if buildId or "build" not in info:
      section = "run"
    else:
      section = "build"

    for resource in info.get('install', []) + info.get(section, {}).get('install', []):
      data = {"items": []}

      subObj = self.retrieve(uuid = resource.get('id'), revision = resource.get('revision'), person = person)

      if subObj is None:
        ObjectManager.Log.error("Cannot find resource with id %s" % (resource.get('id')))
        return []

      # Read 'to' field to understand where the resource would be installed
      # And use that to find the relative path within the resource
      installPath = os.path.normpath(resource.get('to', 'package'))
      destinationPath = os.path.normpath(os.path.join("/", os.path.normpath(os.path.dirname(installPath))))
      installationPath = os.path.normpath(os.path.join("/", os.path.normpath(installPath)))

      if destinationPath == path:
        item = self.resources.retrieveFileStat(resource.get('id'), resource.get('revision'), subObj.resource)

        if item:
          item["name"] = os.path.basename(installPath)
          data['items'].append(item)
          ret.append((subObj, data, None,))

      data = {"items": []}

      if path.startswith(installationPath):
        item = self.resources.retrieveFileStat(resource.get('id'), resource.get('revision'), subObj.resource)

        if item and item["type"] == "tree":
          # List the directory within the resource
          subPath = path[len(installationPath):]
          data['items'].extend(self.resources.retrieveDirectoryFrom(resource.get('id'), resource.get('revision'), subObj.resource, subPath)['items'])
          ret.append((subObj, data, subPath,))

      data = {"items": []}

      unpackPath = resource.get('actions', {}).get('unpack')
      if unpackPath:
        unpackPath = os.path.normpath(os.path.join("/", unpackPath))

      if unpackPath and os.path.commonpath([unpackPath, path]) == unpackPath:
        internalPath = path[len(unpackPath)-1:]

        try:
          items = self.retrieveDirectoryFrom(subObj, internalPath, buildId = buildId)
        except:
          items = {}

        # Incorporate the resource provenance to each entry:
        for item in items.get('items', []):
          if 'from' in items:
            item['from'] = items['from']

        data['items'].extend(items.get('items', []))

        ret.append((subObj, data, internalPath,))

    return ret

  def retrieveDirectoryFrom(self, obj, path, includeResources=False, person=None, buildId = None):
    """ Pulls out the directory structure for the given path.
    """

    if obj.resource:
      return self.resources.retrieveDirectoryFrom(obj.uuid, obj.revision, obj.resource, path)

    pathNotFound = False

    items = {"items": []}

    try:
      if buildId is None and obj.path:
        items = GitRepository(obj.path).retrieveDirectory(path)
      else:
        items = self.storage.retrieveDirectory(obj.uuid, obj.revision, path=path, buildId = buildId)
    except IOError:
      pathNotFound = True

    items['items'] = items.get('items', [])

    if includeResources:
      subItems = self.retrieveDirectoryWithResourcesFrom(obj, path, person = person, buildId = buildId)
      for item in subItems:
        # TODO: Merge names that already exist?
        items['items'].extend(item[1].get('items', []))
        pathNotFound = False

    if pathNotFound:
      raise IOError("File not found")

    return items

  def retrieveFileStatFrom(self, obj, path, includeResources=False, person=None, fromObject=False, buildId = None):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    owner_uuid = self.notes.resolveOwner(obj.uuid)

    ret = None

    if obj.resource and path and not fromObject:
      ret = self.resources.retrieveFileStatFrom(owner_uuid, obj.revision, obj.resource, path)
    else:
      revision = obj.revision

      if obj.resource and fromObject:
        revision = "HEAD"

      # We look for resources first because they override the contents of the object (usually?)
      if includeResources:
        subItems = self.retrieveDirectoryWithResourcesFrom(obj, os.path.dirname(path), person = person, buildId = buildId)
        for item in subItems:
          subObj       = item[0]
          listing      = item[1]
          internalPath = item[2]

          for entry in listing.get('items', []):
            if entry["name"] == os.path.basename(path):
              if internalPath:
                relativePath = os.path.join(internalPath, entry["name"])
                ret = self.resources.retrieveFileStatFrom(subObj.uuid, subObj.revision, subObj.resource, relativePath)
              else:
                # This is a resource itself
                ret = self.resources.retrieveFileStat(subObj.uuid, subObj.revision, subObj.resource)

    if ret is None and obj.path:
      try:
        ret = GitRepository(obj.path, revision=revision).retrieveFileStat(path)
      except IOError:
        pass

    if ret is None:
      if path == "/":
        ret = {"type": "tree", "name": "/", "size": 0}
      else:
        ret = self.storage.retrieveFileStat(owner_uuid, revision, path=path)

    # MIME Type
    ret["mime"] = ["application/octet-stream"]

    import mimetypes

    mimes = mimetypes.MimeTypes()
    ret["mime"] = [mimes.guess_type(ret["name"])[0]]
    if ret["mime"][0] is None:
      ret["mime"] = []
    ret["mime"].append("application/octet-stream")

    return ret

  def retrieveFileFrom(self, obj, path, includeResources=False, person=None, fromObject=False, buildId = None):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    if obj.resource and path and not fromObject:
      return self.resources.retrieveFileFrom(obj.uuid, obj.revision, obj.resource, path)

    revision = obj.revision

    if obj.resource and fromObject:
      revision = "HEAD"

    # We look for resources first because they override the contents of the object (usually?)
    if includeResources:
      subItems = self.retrieveDirectoryWithResourcesFrom(obj, os.path.dirname(path), person = person, buildId = buildId)
      for item in subItems:
        subObj       = item[0]
        listing      = item[1]
        internalPath = item[2]

        for entry in listing.get('items', []):
          if entry["name"] == os.path.basename(path):
            if internalPath:
              # This is within a resource
              relativePath = os.path.join(internalPath, entry["name"])
              return self.resources.retrieveFileFrom(subObj.uuid, subObj.revision, subObj.resource, relativePath)
            else:
              # This is a resource itself
              return self.resources.retrieveFile(subObj.uuid, subObj.revision, subObj.resource)

    if obj.path:
      try:
        return GitRepository(obj.path, revision=revision).retrieveFile(path)
      except IOError:
        pass

    return self.storage.retrieveFile(obj.uuid, revision, path=path)

  def retrieveJSONFrom(self, obj, path, includeResources=False, person=None, fromObject=False):
    """ Pulls out a JSON document at the given path for this object.
    """
    import codecs
    import hashlib
    from collections import OrderedDict

    m = hashlib.sha1()
    m.update(os.path.join(obj.path, path).encode('utf8'));

    indexes = "%s-%s" % (len(obj.roots or []), obj.position or 0)

    cacheTag = "%s-%s-%s-%s-%s" % (obj.uuid, obj.revision, indexes, m.hexdigest(), includeResources)
    data = self.cache.get(cacheTag)

    if not data is None:
      return json.loads(codecs.decode(data, 'utf8'), object_pairs_hook=OrderedDict)

    data = self.retrieveFileFrom(obj, path, includeResources, person, fromObject)
    data = json.loads(codecs.decode(data, 'utf8'), object_pairs_hook=OrderedDict)

    # TODO: understand when information should be cached...
    #       we must always do access control
    if not data is None:
      self.cache.set(cacheTag, json.dumps(data))
    return data

  def ownerFor(self, obj, person):
    """ Retrieves the Object that is the owner of the given object.
    """

    # Check for owner uuid
    owner_uuid = self.notes.resolveOwner(obj.uuid)

    return self.retrieve(owner_uuid, revision = obj.revision, person = person)

  def ownerInfoFor(self, obj):
    """ Pull object info from the owner of the given object.

    Pull object info from the owner of the given object.
    If the object is unowned, then it will pull its own object info.
    """

    if obj.ownerInfo is None:
      try:
        obj.ownerInfo = obj.ownerInfo or self.retrieveJSONFrom(obj, "object.json", fromObject=True, includeResources=False)
      except json.decoder.JSONDecodeError as e:
        raise ObjectJSONError(e)

      obj.ownerInfo = ObjectInfo(obj.ownerInfo)

    return obj.ownerInfo

  def objectPositionWithin(self, obj, subObject, revision = None):
    """ Returns the position of the given subObject within the given object.

    When revision is not given, this will return the first index for any
    revision of the object that exists within the list. Otherwise, it will only
    return for the exact revision.
    """

    info = self.infoFor(obj)
    for i,dependency in enumerate(info.get('contains', [])):
      if dependency.get('id') == subObject.uuid and (revision is None or dependency.get('revision') == revision):
        return i

    return None

  def infoFor(self, obj):
    """ Pull object info from revision
    """

    if obj is None:
      return None

    if obj.revision == obj.infoRevision and obj.info is not None:
      return obj.info

    obj.infoRevision = obj.revision

    obj.info = obj.info or self.ownerInfoFor(obj)

    if obj.info is None:
      # We don't know how to get the info
      return None

    if obj.uuid and obj.info.get('id') != obj.uuid:
      info = obj.info
      obj.info = {}
      for derivative in info.get('includes', []):
        if derivative.get('id') == obj.uuid:
          new_info = info.copy()

          new_info['install'] = new_info.get('install', []).copy()
          new_info['install'].extend(new_info.get('build', {}).get('install', []))
          new_info['install'].extend(new_info.get('run', {}).get('install', []))

          if new_info['install'] == []:
            del new_info['install']

          if 'includes' in new_info:
            del new_info['includes']
          if 'subtype' in new_info:
            del new_info['subtype']
          if 'inputs' in new_info:
            del new_info['inputs']
          if 'outputs' in new_info:
            del new_info['outputs']
          if 'build' in new_info:
            del new_info['build']
          if 'run' in new_info:
            del new_info['run']
          if 'configurations' in new_info:
            del new_info['configurations']
          if 'include' in new_info:
            del new_info['include']

          new_info.update(derivative)
          new_info['owner'] = {
            'id': obj.ownerInfo.get('id')
          }
          obj.info = new_info
          break

    # Inject the selected 'file'
    if obj.file:
      obj.info['file'] = obj.file

    return obj.info

  def install(self, obj, basePath, build = False):
    """ Installs resources from within the object to the given path.

    The final path for the resource is derived from the given base path and the
    ``to`` attribute in the resource info.
    """

    objectInfo = self.infoFor(obj)

    if objectInfo is None:
      return

    resources = objectInfo.get('install', [])

    if build and 'build' in objectInfo:
      resources.extend(objectInfo['build'].get('install', []))
    else:
      resources.extend(objectInfo.get('run', {}).get('install', []))

    resourceInfo = self.resources.installAll({"install": resources}, basePath)

    return resourceInfo

  def searchTypes(self, query):
    """ Returns a list with a string for each type that matches the given query.
    """

    return self.datastore.retrieveObjectTypes(search = query)

  def search(self, uuid = None, name = None, object_type=None):
    """ Returns a list with an ObjectRecord for each object found for the given terms.
    """

    return self.datastore.retrieveObjects(uuid = uuid, name = name, object_type = object_type)

  def viewersFor(self, viewsType, viewsSubtype = None):
    """ Returns rows of objects that can view the given type and subtype.
    """

    return self.datastore.viewersFor(viewsType, viewsSubtype)

  def configuratorsFor(self, configures_uuid):
    """ Returns rows of objects that can configure the given object.
    """

    return self.datastore.configuratorsFor(configures_uuid)

class ObjectError(Exception):
  """ Base class for all object errors.
  """
  pass

class ObjectJSONError(ObjectError):
  """ JSON encoding error.
  """

  def __init__(self, jsonDecodeError):
    """ Creates a new ObjectJSONError.
    """
    self.message      = jsonDecodeError.msg
    self.lineNumber   = jsonDecodeError.lineno
    self.columnNumber = jsonDecodeError.colno
    self.position     = jsonDecodeError.pos
    self.filename     = jsonDecodeError.doc

    self.endOfFile = False
    if self.position == len(self.filename):
      self.endOfFile = True

    self.context = self.explanation()

    endOfFileMessage = ""
    if self.endOfFile:
      endOfFileMessage = " (end of file)"

    self.report = "line {}, col {}{}: {}".format(self.lineNumber, self.columnNumber, endOfFileMessage, self.message)

  def explanation(self):
    """ Returns a string pointing to the problem.
    """

    import re

    # Get 2 context lines
    self.filename = self.filename + " "
    lines = self.filename.split("\n")

    # Friendlier Error Detection(tm)
    redefineErrorAtCharacter = None
    if self.message == "Expecting ',' delimiter":
      if self.endOfFile:
        self.message = "Expecting a terminating brace '}' at the end of the document"
      else:
        # Look for the last non-whitespace
        redefineErrorAtCharacter = r'\S\s*$'
    elif self.message == "Expecting property name enclosed in double quotes" or self.message == "Expecting value":
      # Generally this is the presence of a terminating comma
      # Check for this by seeing { at position
      if self.filename[self.position] == "}":
        self.message = "Redundant comma found before a terminating brace"
        redefineErrorAtCharacter = ","
      elif self.filename[self.position] == "]":
        self.message = "Redundant comma found before the end of a list"
        redefineErrorAtCharacter = ","

    if redefineErrorAtCharacter:
      # Find the comma
      location = None
      newLineNumber = self.lineNumber
      newColNumber  = self.columnNumber
      try:
        location = re.search(redefineErrorAtCharacter, lines[self.lineNumber-1][:self.columnNumber-1]).end()
      except:
        newLineNumber -= 1

      if location is None:
        for line in reversed(lines[:self.lineNumber-1]):
          try:
            location = re.search(redefineErrorAtCharacter, line).end()
            break
          except:
            newLineNumber -= 1

      if location is not None:
        self.columnNumber = location + 1
        self.lineNumber = newLineNumber

    first = self.lineNumber - 3
    if first < 0:
      first = 0

    beginLines = lines[first:self.lineNumber-1]
    endLines   = lines[self.lineNumber:self.lineNumber+3]
    arrow      = "-" * (self.columnNumber) + "~^\n"
    if len(endLines) == 0 or (len(endLines) == 1 and endLines[0] == ""):
      arrow = arrow.rstrip()

    currentLine = lines[self.lineNumber-1]
    start  = currentLine[:self.columnNumber-1]
    if self.columnNumber == len(currentLine) + 1:
      middle = ""
      end = ""
    else:
      middle = currentLine[self.columnNumber-1]
      end    = currentLine[self.columnNumber:]

    from termcolor import colored

    return "  {}\n  {}{}{}\n{}  {}".format("\n  ".join(beginLines).rstrip(), start, colored(middle, 'red', attrs=['bold']), end, colored(arrow, 'white', attrs=['bold']), "\n  ".join(endLines).rstrip())

  def __str__(self):
    return self.report + "\n" + self.context
