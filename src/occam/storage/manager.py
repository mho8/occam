# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log            import loggable
from occam.git_repository import GitRepository

import shutil
import json
import os
import codecs
import subprocess

from occam.manager import uses, manager

from occam.notes.manager import NoteManager

@loggable
@uses(NoteManager)
@manager("storage")
class StorageManager:
  """
  This OCCAM manager handles the storage backend.
  """

  handlers = {}
  priorities  = []
  handlerImpl = []
  instantiated = {}

  def __init__(self):
    import occam.storage.plugins.local
    import occam.storage.plugins.ipfs

  @staticmethod
  def register(storageName, handlerClass, priority=100):
    """
    Adds a new storage backend type.
    """

    import bisect

    index = bisect.bisect(StorageManager.priorities, priority)
    StorageManager.priorities.insert(index, priority)
    StorageManager.handlerImpl.insert(index, storageName)
    StorageManager.handlers[storageName] = handlerClass

  def handlerFor(self, storageName):
    """ Returns an instance of a handler for the given name.
    """

    if not storageName in StorageManager.handlers:
      StorageManager.Log.error("storage backend %s not known" % (storageName))
      return None

    # Instantiate a storage backend if we haven't seen it before
    if not storageName in StorageManager.instantiated:
      # Pull the configuration (from the 'stores' subpath and keyed by the name)
      subConfig = self.configurationFor(storageName)

      # Create a driver instance
      instance = StorageManager.handlers[storageName](subConfig)

      # If there is a problem detecting the backend, cancel
      # This will set the value in the instantiations to None
      # TODO: cache this in the stores configuration? or detection file?
      if hasattr(instance, 'detect') and callable(instance.detect) and not instance.detect():
        instance = None

      # Set the instance
      StorageManager.instantiated[storageName] = instance

    if StorageManager.instantiated[storageName] is None:
      # The driver could not be initialized
      return None

    return StorageManager.instantiated[storageName]

  def configurationFor(self, storageName):
    """
    Returns the configuration for the given storage that is found within the
    occam configuration (config.yml) under the "stores" section under the
    given storage backend name.
    """

    config = self.configuration
    subConfig = config.get(storageName, {})

    return subConfig

  # Interface follows

  def initialize(self):
    """
    """

    ret = {}
    for k, v in StorageManager.handlers.items():
      subConfig = self.configurationFor(k)
      handler = self.handlerFor(k)
      if handler:
        info = handler.initialize(subConfig)
        if not info is None:
          ret[k] = info

    return ret

  def discoverNode(self, info):
    """
    """

    for k, v in StorageManager.handlers.items():
      if k in info:
        self.handlerFor(k).discoverNode(info[k])

  def discover(self, uuid, revision=None):
    """ Returns a list of hosts where the given object is known to exist.
    """

    ret = []
    for k, v in StorageManager.handlers.items():
      handler = self.handlerFor(k)
      if handler:
        ret.extend(handler.discover(uuid))

    return ret

  def isLocal(self, uuid):
    """
    Returns whether or not the given object exists locally.
    """

    for k, v in StorageManager.handlers.items():
      handler = StorageManager.handlerFor(k)
      if handler:
        if handler.isLocal(uuid):
          return True

    return False

  def isKnown(self, uuid, revision):
    """
    Returns whether or not the given revision of the given object is known.
    This is generally used when a given revision is needed in a complete state.
    If it is not known directly, it will try the latest revision and will need
    to clone that object and pull out the revision in a writable path somewhere.

    If it is known, however, it can simply be read using the versioning of the
    file system itself.
    """

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.notes.retrieveInvocation(uuid, category="stores/%s" % (k), key="store", revision=revision)
      if len(storageInfo) > 0:
        return True

    return None

  def buildPathFor(self, uuid, buildId, revision, create=False):
    """ Retrieves a path for the built version of the object at the given revision.
    """

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.notes.retrieve(uuid, category="stores/%s/builds" % (k), key="build-%s" % (buildId), revision=revision)
      if create or (not storageInfo is None and len(storageInfo) > 0):
        handler = self.handlerFor(k)
        if handler:
          ret = handler.buildPathFor(uuid, storageInfo, buildId, revision=revision, create=create)
          if not ret is None:
            return ret

    return None

  def repositoryPathFor(self, uuid, revision, create=False):
    """ Retrieves a path for the object's data repository.
    """

    for k in self.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        ret = handler.repositoryPathFor(uuid, create=create)

        if not ret is None:
          return ret

  def resourcePathFor(self, uuid, create=False):
    """ Retrieves and/or creates the object's resource path.

    The resource path is some location to keep large (usually hashable) file
    data associated with the object.
    """

    for k in self.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        ret = handler.resourcePathFor(uuid, create=create)

        if not ret is None:
          return ret

  def pathFor(self, uuid, revision, buildId=None, create=False):
    """ Retrieves a path that one can read the given object at the given revision.

    If create is True, the revision will be instantiated if no knowledge of it
    is known and no local cached copy of it is available. If it is created, it
    will be subsequently cached and stored.
    """

    for k in self.handlerImpl:
      if buildId:
        storageInfo = self.notes.retrieve(uuid, category="stores/%s/builds" % (k), key="build-%s" % (buildId), revision=revision)

        if storageInfo:
          storageInfo = storageInfo[0]['value']
      else:
        storageInfo = self.storageHashFor(uuid, revision, k)

      if not storageInfo is None and len(storageInfo) > 0:
        handler = self.handlerFor(k)
        if handler:
          if buildId:
            ret = handler.buildPathFor(uuid, storageInfo, buildId=buildId, create=create, revision=revision)
          else:
            ret = handler.pathFor(storageInfo, revision=revision, create=create) 

        if not ret is None:
          return ret

    return None

  def pin(self, uuid):
    """
    Ensures that the object is preserved locally and not garbage collected when
    it is known to be replicated offsite.
    """

    for k, v in StorageManager.handlers.items():
      handler = self.handlerFor(k)
      if handler:
        if handler.pin(uuid):
          return True

    return False

  def purge(self, uuid):
    """
    Removes the given object from the local storage. Returns False if any
    storage mechanism could not remove the object and it persists.
    """

    for k in StorageManager.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        if not handler.purge(uuid):
          return False

    return True

  def pull(self, uuid):
    """
    """

    for k in StorageManager.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        if handler.pull(uuid):
          return True

    return False

  def push(self, uuid, revision, path):
    """ Stores the given object located at the given path.
    """

    hashes = {}

    for k in self.handlerImpl:
      StorageManager.Log.noisy("Pushing to %s from %s" % (k, path))
      handler = self.handlerFor(k)
      if handler:
        storageHash, storedRevision = handler.push(uuid, path, revision)
        hashes[k] = storageHash

        if k != "local":
          self.notes.store(uuid, "stores/%s" % (k), key="store", value=storageHash, revision=storedRevision)

    return hashes

  def pushResource(self, uuid, revision, path):
    """ Stores the given resource data located at the given path.

    Resource paths might be specific to their revision, or it may be the root
    path for an aggregate resource (such as a it repository)
    """

    hashes = {}
    for k in self.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        storageHash = handler.pushResource(uuid, revision, path)
        if not storageHash is None:
          hashes[k] = storageHash
          self.notes.store(uuid, "stores/%s" % (k), key="resource", value=storageHash, revision=revision)

    return hashes

  def pushBuild(self, uuid, revision, buildId, path):
    """ Stores the given object build located at the given path.
    """

    hashes = {}
    for k in self.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        storageHash = handler.pushBuild(uuid, revision, buildId, path)
        if not storageHash is None:
          hashes[k] = storageHash
          self.notes.store(uuid, "stores/%s/builds" % (k), key="build-%s" % (buildId), value=storageHash, revision=revision)

    return hashes

  def instantiate(self, uuid, revision, path):
    """ Places a copy of the given object at the given revision to the given path.

    The directory specified by path must not exist.
    """

    repositoryPath = None
    revisionPath   = None

    # Get the path for the given revision
    if repositoryPath is None:
      repositoryPath = self.handlerFor('local').pathFor({'uuid': uuid})
    if revisionPath is None:
      revisionPath = self.pathFor(uuid, revision=revision)

    ret = None

    if not revisionPath is None:
      # Copy this path
      StorageManager.Log.noisy("Cloning from %s" % (revisionPath))

      try:
        git = GitRepository(revisionPath, revision=revision)
        git.clone(path)

        newGitRepository = GitRepository(path, revision=revision)
        newGitRepository.reset(revision, hard=True)

        ret = path

        return ret
      except:
        pass

    if not repositoryPath is None:
      # Clone from this path
      StorageManager.Log.noisy("Cloning from %s" % (repositoryPath))

      git = GitRepository(repositoryPath)
      git.clone(path)

      newGitRepository = GitRepository(path, revision=revision)
      newGitRepository.reset(revision, hard=True)

      ret = path

      # TODO: Push revision back since we have never seen it before

    if not ret is None:
      return ret

    return None

  def clone(self, uuid, revision, path):
    """ Clones the object to the specifed local filesystem path.
    """

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.storageHashFor(uuid, revision, k)
      if storageInfo is None:
        # We don't know how to retrieve from this storage.
        continue

      handler = self.handlerFor(k)
      if handler:
        ret = handler.clone(storageInfo, path)
        if not ret is None:
          return ret

    return None

  def storageHashFor(self, uuid, revision, storageType):
    """ Retrieves the storage information for the particular object/revision pair.
    """

    storageInfo = self.notes.retrieve(uuid, category="stores/%s" % (storageType), key="store", revision=revision)
    if storageType != "local" and (storageInfo is None or len(storageInfo) == 0):
      # We don't know how to retrieve from this storage.
      return None

    storageHash = {"uuid": uuid, "revision": revision}
    if storageInfo:
      storageHash = storageInfo[0]["value"]

    return storageHash

  def retrieveHistory(self, uuid, revision, path = None):
    """ Retrieves an object history from the stored object at the given revision.
    """

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.storageHashFor(uuid, revision, k)
      if storageInfo is None:
        # We don't know how to retrieve from this storage.
        continue
      try:
        handler = self.handlerFor(k)
        if handler:
          ret = handler.retrieveHistory(storageInfo, revision, path)
      except IOError:
        ret = None

      if not ret is None:
        return ret

    raise IOError("File Not Found")

  def retrieveDirectory(self, uuid, revision, path, buildId = None):
    """ Retrieves a directory listing for the given path by any means.
    """

    if path.startswith("/"):
      path = path[1:]

    hashes = {}
    for k in self.handlerImpl:
      if buildId:
        storageInfo = self.notes.retrieve(uuid, category="stores/%s/builds" % (k), key="build-%s" % (buildId), revision=revision)
      else:
        storageInfo = self.notes.retrieve(uuid, category="stores/%s" % (k), key="store", revision=revision)

      if storageInfo is None or len(storageInfo) == 0:
        # We don't know how to retrieve from this storage.
        continue
      try:
        handler = self.handlerFor(k)
        if handler:
          ret = handler.retrieveDirectory(storageInfo[0]["value"], revision, path, buildId = buildId)
      except IOError:
        ret = None

      if not ret is None:
        return ret

    raise IOError("File Not Found")

  def retrieveFileStat(self, uuid, revision, path, buildId = None):
    """ Retrieves the file status by any means.
    """

    # TODO: return a stream

    if path.startswith("/"):
      path = path[1:]

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.storageHashFor(uuid, revision, k)
      if storageInfo is None:
        # We don't know how to retrieve from this storage.
        continue

      StorageManager.Log.noisy("attempting to retrieve file from %s" % (k))
      handler = self.handlerFor(k)

      ret = None

      if handler:
        try:
          ret = handler.retrieveFileStat(storageInfo, revision, path, buildId = buildId)
        except IOError:
          ret = None

      if not ret is None:
        return ret

    raise IOError("File Not Found")

  def retrieveFile(self, uuid, revision, path, buildId = None):
    """ Retrieves the file by any means.
    """

    # TODO: return a stream

    if path.startswith("/"):
      path = path[1:]

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.storageHashFor(uuid, revision, k)
      if storageInfo is None:
        # We don't know how to retrieve from this storage.
        continue

      StorageManager.Log.noisy("attempting to retrieve file from %s" % (k))
      handler = self.handlerFor(k)

      ret = None

      if handler:
        try:
          ret = handler.retrieveFile(storageInfo, revision, path, buildId = buildId)
        except IOError:
          ret = None

      if not ret is None:
        return ret

    raise IOError("File Not Found")

  def retrieveJSON(self, uuid, revision, path, buildId = None):
    """ Retrieves JSON data by any means.
    """

    from collections import OrderedDict

    data = self.retrieveFile(uuid, revision, path, buildId = buildId)

    return json.loads(data.decode('utf8'), object_pairs_hook=OrderedDict)

def storage(name, priority=100):
  """
  This decorator will register the given class as a storage backend.
  """
  def register_store(cls):
    StorageManager.register(name, cls, priority=priority)
    cls = loggable("StorageManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_store
